package com.hnttechs.www.mdbuyer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dell on 8/1/16.
 */
public class Activity_Customer_Info extends ActionBarActivity {

    static TextView txt_name;
    static TextView txt_email;
    static TextView txt_phone;
    static TextView txt_city;
    static TextView txt_township;
    static TextView txt_address;
    static SharedPreferences mPreferences;
    static ArrayList<String> product_id;
    static ArrayList<String> var_qty;
    static ArrayList<String> product_name;
    ViewPager viewpager;
    static String str_product_id;
    static String str_qty;
    static String str_product_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fill_customer_info);


        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        txt_name = (TextView)findViewById(R.id.txt_name);
        txt_email = (TextView)findViewById(R.id.txt_email);
        txt_phone = (TextView)findViewById(R.id.txt_phone);
        txt_city = (TextView)findViewById(R.id.txt_city);
        txt_township = (TextView)findViewById(R.id.txt_township);
        txt_address = (TextView)findViewById(R.id.txt_address);

        mPreferences = getSharedPreferences("CurrentUser", Context.MODE_PRIVATE);

        viewpager = HomeFragment.viewPager;
        Intent i = getIntent();
        product_id = i.getStringArrayListExtra("product_id");
        var_qty = i.getStringArrayListExtra("qty");
//        product_name = i.getStringArrayListExtra("product_name");

        str_product_id="[";
        for(int j=0;j<product_id.size();j++) {
            if (j==product_id.size()-1) {
                str_product_id = str_product_id + product_id.get(j).toString() + "]";
            } else if (product_id.size()!=2  && j==product_id.size()-2) {
                str_product_id = str_product_id + product_id.get(j).toString();
            }  else if (product_id.size()-2 == 1) {
                str_product_id = "," + product_id.get(j).toString();
            } else {
                str_product_id = str_product_id + product_id.get(j).toString() + ",";
            }
        }


        str_qty="[";
        for(int j=0;j<var_qty.size();j++) {
            if (j==var_qty.size()-1) {
                str_qty = str_qty + var_qty.get(j).toString() + "]";
            } else if (var_qty.size()!=2  && j==var_qty.size()-2) {
                str_qty = str_qty + var_qty.get(j).toString();
            }  else if (var_qty.size()-2 ==1) {
                str_qty = "," + str_qty + var_qty.get(j).toString();
            } else{
                str_qty = str_qty + var_qty.get(j).toString() + ",";
            }
        }

//        str_product_name="[";
//        for(int j=0;j<product_name.size();j++) {
//            if (j==product_name.size()) {
//                str_product_name = str_product_name + product_name.get(j).toString() + "]";
//            } else {
//                str_product_name = str_product_name + product_name.get(j).toString() + ",";
//            }
//        }

        Button btn_check_out = (Button)findViewById(R.id.btn_check_out);
        btn_check_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendPostRequest(txt_name.getText().toString(), txt_email.getText().toString(),
                        txt_phone.getText().toString(), txt_city.getText().toString(), txt_township.getText().toString(),
                        txt_address.getText().toString(), str_product_id, mPreferences.getString("UserId", "0"), str_qty);
            }
        });

    }

    private void sendPostRequest(String Name,
                                 String Email,
                                 String Phone,
                                 String City,
                                 String Township,
                                 String Address,
                                 String ProductId,
                                 String UserId,
                                 String Quantity) {

        class SendPostReqAsyncTask extends AsyncTask<Object, Void, String> {

            @Override
            protected String doInBackground(Object... params) {

                String paramName = (String) params[0];
                String paramEmail = (String) params[1];
                String paramPhone = (String) params[2];
                String paramCity = (String) params[3];
                String paramTownship = (String) params[4];
                String paramAddress = (String) params[5];
                String paramProductId = (String) params[6];
                String paramUserId = (String) params[7];
                String paramQuantity = (String) params[8];

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost("http://www.myanmardealer.com/to_send_shipment");

                BasicNameValuePair nameBasicNameValuePair = new BasicNameValuePair("customer_name", paramName);
                BasicNameValuePair emailBasicNameValuePAir = new BasicNameValuePair("customer_email", paramEmail.toString());
                BasicNameValuePair phoneBasicNameValuePAir = new BasicNameValuePair("customer_phone", paramPhone.toString());
                BasicNameValuePair cityBasicNameValuePair = new BasicNameValuePair("customer_city", paramCity);
                BasicNameValuePair townshipBasicNameValuePAir = new BasicNameValuePair("customer_township", paramTownship.toString());
                BasicNameValuePair addressBasicNameValuePAir = new BasicNameValuePair("customer_address", paramAddress.toString());
                BasicNameValuePair productIdBasicNameValuePair = new BasicNameValuePair("product_id", paramProductId.toString());
                BasicNameValuePair userIdBasicNameValuePAir = new BasicNameValuePair("user_id", paramUserId);
                BasicNameValuePair quantityBasicNameValuePAir = new BasicNameValuePair("quantity", paramQuantity.toString());

                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(nameBasicNameValuePair);
                nameValuePairList.add(emailBasicNameValuePAir);
                nameValuePairList.add(phoneBasicNameValuePAir);
                nameValuePairList.add(cityBasicNameValuePair);
                nameValuePairList.add(townshipBasicNameValuePAir);
                nameValuePairList.add(addressBasicNameValuePAir);
                nameValuePairList.add(productIdBasicNameValuePair);
                nameValuePairList.add(userIdBasicNameValuePAir);
                nameValuePairList.add(quantityBasicNameValuePAir);

                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);
                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        InputStream inputStream = httpResponse.getEntity().getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;

                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
                        return stringBuilder.toString();
                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
//                dialog.dismiss();

                ShoppingCartHelper.removeProduct(ShoppingCart_GridViewAdapter.clothing);
                viewpager.setCurrentItem(0);

                finish();
            }
        }
        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(Name, Email, Phone,City, Township, Address, ProductId, UserId, Quantity);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;
            case R.id.action_message:
                Intent message = new Intent(getBaseContext(),Message_Box.class);
                startActivity(message);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
