package com.hnttechs.www.mdbuyer;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 8/1/16.
 */
public class Activity_Shop extends ActionBarActivity {

    GridView gdv_product;
    BigDeal_GridViewAdapter bigdeal_adapter;
    static String original_store_name;
    static String store_name;

    BigDeal_GridViewAdapter adapter;
    static String serverData;
    ArrayList<HashMap<String, String>> arraylist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shop_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        ImageView img_shop_icon = (ImageView)findViewById(R.id.img_shop_icon);
        TextView txt_shop_name = (TextView)findViewById(R.id.txt_shop_name);

        Intent i = getIntent();
        original_store_name = i.getStringExtra("store_name");
        store_name = original_store_name.replace(" ","%20");

        txt_shop_name.setText(original_store_name);
        gdv_product = (GridView) findViewById(R.id.gdv_product);

        new DataFetcherTask().execute();

    }



    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            serverData = null;

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://www.myanmardealer.com/get_store_product?store_name=" + store_name);

//            HttpGet httpGet = new HttpGet("http://www.myanmardealer.com/get_store_product?store_name="?);

            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                serverData = EntityUtils.toString(httpEntity);
                Log.d("response", serverData);

                JSONObject jsonObject = new JSONObject(serverData);
                JSONArray jsonArray = jsonObject.getJSONArray("Products");
                arraylist = new ArrayList<HashMap<String, String>>();

                for (int i = 0; i < jsonArray.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    JSONObject jsonObjectClothing = jsonArray.getJSONObject(i);

                    map.put("clothingName", jsonObjectClothing.getString("title"));
                    map.put("clothingPrice", jsonObjectClothing.getString("price"));
                    map.put("clothingQuantity", jsonObjectClothing.getString("quantity"));
                    map.put("clothingStore_name", jsonObjectClothing.getString("store_name"));
                    map.put("size", jsonObjectClothing.getString("size"));
                    map.put("color", jsonObjectClothing.getString("color"));
                    map.put("description", jsonObjectClothing.getString("description"));
                    map.put("made_by_country", jsonObjectClothing.getString("made_by_country"));
                    map.put("brand", jsonObjectClothing.getString("brand"));
                    map.put("cloth_photo", jsonObjectClothing.getString("avatar1_file_name"));
                    map.put("user_id", jsonObjectClothing.getString("user_id"));

                    arraylist.add(map);
                }

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(arraylist != null) {
                bigdeal_adapter = new BigDeal_GridViewAdapter(getBaseContext(), arraylist, false);
                gdv_product.setAdapter(bigdeal_adapter);
                gdv_product.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent i = new Intent(getBaseContext(), ProductDetailActivity.class);
                        i.putExtra("Position", position);
                        i.putExtra("arraylist", arraylist);
                        i.putExtra("category", "clothing");
                        startActivity(i);
                    }
                });
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;
            case R.id.action_message:
                Intent message = new Intent(getBaseContext(),Message_Box.class);
                startActivity(message);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
