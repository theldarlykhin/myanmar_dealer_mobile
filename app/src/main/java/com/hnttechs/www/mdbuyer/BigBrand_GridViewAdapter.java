package com.hnttechs.www.mdbuyer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 7/15/15.
 */
public class BigBrand_GridViewAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<HashMap<String, String>> serverdata_Arraylist;
    private boolean mShowQuantity;
    HashMap<String, String> resultp = new HashMap<String, String>();

    public BigBrand_GridViewAdapter(Context context, ArrayList<HashMap<String, String>> data_arrayList, Boolean showQuantity) {
        this.context        = context;
        this.serverdata_Arraylist = data_arrayList;
        mShowQuantity       = showQuantity;
    }

    @Override
    public int getCount() { return serverdata_Arraylist.size(); }

    @Override
    public Object getItem(int position) { return null; }

    @Override
    public long getItemId(int position) { return 0; }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if (convertView == null) {
            gridView = new View(context);
            gridView = inflater.inflate(R.layout.gdv_item_bigbrand, null);
            resultp = serverdata_Arraylist.get(position);

            ImageView img_brand_photo = (ImageView) gridView.findViewById(R.id.img_brand_photo);

            img_brand_photo.setImageResource(R.drawable.prada);
        } else { gridView = (View) convertView; }
        return gridView;
    }
}
