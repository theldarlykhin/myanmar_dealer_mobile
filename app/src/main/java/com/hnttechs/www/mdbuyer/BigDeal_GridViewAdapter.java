package com.hnttechs.www.mdbuyer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 7/15/15.
 */
public class BigDeal_GridViewAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<HashMap<String, String>> serverdata_Arraylist;
    private boolean mShowQuantity;
    HashMap<String, String> resultp = new HashMap<String, String>();
    ImageLoader_cartoon imageLoader = new ImageLoader_cartoon(context);
    static String image_string1;

    public BigDeal_GridViewAdapter(Context context, ArrayList<HashMap<String, String>> data_arrayList, Boolean showQuantity) {
        this.context = context;
        this.serverdata_Arraylist = data_arrayList;
        mShowQuantity = showQuantity;
    }

    @Override
    public int getCount() {
        return serverdata_Arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if (convertView == null) {
            gridView = new View(context);
            gridView = inflater.inflate(R.layout.gdv_item_bigdeal, null);
            resultp = serverdata_Arraylist.get(position);

            ImageView img_cloth_photo = (ImageView) gridView.findViewById(R.id.img_cloth_photo);
            TextView lbl_price = (TextView) gridView.findViewById(R.id.lbl_price);
            TextView lbl_name = (TextView) gridView.findViewById(R.id.lbl_name);

            lbl_name.setText(resultp.get("clothingName"));
            lbl_price.setText(resultp.get("clothingPrice"));

//if(position == 1) {
//            if (resultp.get("avatar1_edit") != null && !resultp.get("avatar1_edit").isEmpty() && !resultp.get("avatar1_edit").equals("null")) {
//                image_string1 = resultp.get("avatar1_edit").toString();
//            }
//            try {
//                if (image_string1 != null && !image_string1.isEmpty() && !image_string1.equals("null")) {
//                    String base64_string = image_string1.substring(22, image_string1.length());
//
//                    String base64_string_substring = base64_string.substring(0, base64_string.length() - 1);
//                    byte[] decodedString = Base64.decode(base64_string_substring, Base64.URL_SAFE);
//
//                    Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//                    img_cloth_photo.setImageBitmap(bitmap);

//                }
//
//
//            } catch (IllegalArgumentException e) {
//            }
//            imageLoader.DisplayImage("myanmardealer.com" + resultp.get("cloth_photo"), img_cloth_photo);
        } else {
            gridView = (View) convertView;
        }
        return gridView;
    }
}
