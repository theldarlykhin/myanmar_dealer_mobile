package com.hnttechs.www.mdbuyer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.util.Log;

import com.hnttechs.www.mdbuyer.model.Clothing;

import java.util.ArrayList;

/**
 * Created by dell on 4/8/16.
 */
public class ClothingDbHandler extends SQLiteOpenHelper implements ClothingListener {

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "clothingDatabase1.db";
    private static final String TABLE_NAME = "clothing_table1";
    private static final String KEY_ID = "_id";
    private static final String KEY_TITLE="title";
    private static final String KEY_PRICE="price";
    private static final String KEY_QUANTITY="quantity";
    private static final String KEY_SIZE="size";
    private static final String KEY_MADE_BY_COUNTRY="made_by_country";
    private static final String KEY_MADE_WITH="made_with";
    private static final String KEY_DESCRIPTION="description";
    private static final String KEY_GENDER="gender";
    private static final String KEY_BRAND="brand";
    private static final String KEY_SELLER_ID="seller_id";
    private static final String KEY_COLOR="color";
    private static final String KEY_STORE_NAME="store_name";
    private static final String KEY_AVATER1="avater1";
    private static final String KEY_AVATER2="avater2";
    private static final String KEY_AVATER3="avater3";
    private static final String KEY_AVATER4="avater4";
    private static final String KEY_AVATER5="avater5";

    String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+" ("+KEY_ID+" INTEGER PRIMARY KEY,"
            +KEY_TITLE+" TEXT,"+KEY_PRICE+" INTEGER,"+KEY_QUANTITY+" INTEGER,"+KEY_SIZE+" TEXT,"
    +KEY_MADE_BY_COUNTRY+" TEXT,"+KEY_MADE_WITH+" TEXT,"+KEY_DESCRIPTION+" TEXT,"+KEY_GENDER+" TEXT,"
    +KEY_BRAND+" TEXT," +KEY_SELLER_ID+" INTEGER," +KEY_COLOR+" TEXT,"+KEY_STORE_NAME+" TEXT,"
    +KEY_AVATER1+" TEXT,"+KEY_AVATER2+" TEXT,"+KEY_AVATER3+" TEXT,"+KEY_AVATER4+" TEXT,"
    +KEY_AVATER5+" TEXT)";
    String DROP_TABLE = "DROP TABLE IF EXISTS "+TABLE_NAME;

    public ClothingDbHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE);
        onCreate(db);
    }

    @Override
    public void addClothing(Clothing clothing) {
        SQLiteDatabase db = this.getWritableDatabase();
        try{

//            db.execSQL("delete from "+ TABLE_NAME);

            ContentValues values = new ContentValues();
            values.put(KEY_TITLE, clothing.getTitle());
            values.put(KEY_PRICE, clothing.getPrice());
            values.put(KEY_QUANTITY,clothing.getQuantity());
            values.put(KEY_SIZE,clothing.getSize());
            values.put(KEY_MADE_BY_COUNTRY,clothing.getMade_by_country());
            values.put(KEY_MADE_WITH,clothing.getMade_with());
            values.put(KEY_DESCRIPTION,clothing.getDescription());
            values.put(KEY_GENDER,clothing.getGender());
            values.put(KEY_BRAND,clothing.getBrand());
            values.put(KEY_SELLER_ID,clothing.getSeller_id());
            values.put(KEY_COLOR,clothing.getColor());
            values.put(KEY_STORE_NAME,clothing.getStore_name());
            values.put(KEY_AVATER1,clothing.getAvater1());
            values.put(KEY_AVATER2,clothing.getAvater2());
            values.put(KEY_AVATER3,clothing.getAvater3());
            values.put(KEY_AVATER4,clothing.getAvater4());
            values.put(KEY_AVATER5,clothing.getAvater5());
            db.insert(TABLE_NAME, null, values);
            db.close();
        }catch (Exception e){
            Log.e("problem", e + "");
        }
    }

    @Override
    public ArrayList<Clothing> getAllClothing() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Clothing> clothingList = null;
        try{
            clothingList = new ArrayList<Clothing>();
            String QUERY = "SELECT * FROM "+TABLE_NAME;
            Cursor cursor = db.rawQuery(QUERY, null);
            if(!cursor.isLast())
            {
                while (cursor.moveToNext())
                {
                    Clothing clothing = new Clothing();
                    clothing.setId(cursor.getInt(0));
                    clothing.setTitle(cursor.getString(1))  ;
                    clothing.setPrice(cursor.getInt(2));
                    clothing.setQuantity(cursor.getInt(3));
                    clothing.setSize(cursor.getString(4));
                    clothing.setMade_by_country(cursor.getString(5));
                    clothing.setMade_with(cursor.getString(6));
                    clothing.setDescription(cursor.getString(7));
                    clothing.setGender(cursor.getString(8));
                    clothing.setBrand(cursor.getString(9));
                    clothing.setSeller_id(cursor.getInt(10));
                    clothing.setColor(cursor.getString(11));
                    clothing.setStore_name(cursor.getString(12));
                    clothing.setAvater1(cursor.getString(13));
                    clothing.setAvater2(cursor.getString(14));
                    clothing.setAvater3(cursor.getString(15));
                    clothing.setAvater4(cursor.getString(16));
                    clothing.setAvater5(cursor.getString(17));
                    clothingList.add(clothing);
                }
            }
            db.close();
        }catch (Exception e){
            Log.e("error",e+"");
        }
        return clothingList;
    }

    @Override
    public int getClothingCount() {
        int num = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        try{
            String QUERY = "SELECT * FROM "+TABLE_NAME;
            Cursor cursor = db.rawQuery(QUERY, null);
            num = cursor.getCount();
            db.close();
            return num;
        }catch (Exception e){
            Log.e("error",e+"");
        }
        return 0;
    }

}
