package com.hnttechs.www.mdbuyer;

import com.hnttechs.www.mdbuyer.model.Clothing;

import java.util.ArrayList;

/**
 * Created by dell on 4/8/16.
 */
public interface ClothingListener {

    public void addClothing(Clothing clothing);

    public ArrayList<Clothing> getAllClothing();

    public int getClothingCount();

}
