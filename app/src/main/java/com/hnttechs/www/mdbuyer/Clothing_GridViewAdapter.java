package com.hnttechs.www.mdbuyer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hnttechs.www.mdbuyer.model.Clothing;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 7/15/15.
 */
public class Clothing_GridViewAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<HashMap<String, String>> serverdata_Arraylist;
    private boolean mShowQuantity;
    HashMap<String, String> resultp = new HashMap<String, String>();
    ImageLoader_cartoon imageLoader = new ImageLoader_cartoon(context);

    public Clothing_GridViewAdapter(Context context, ArrayList<HashMap<String, String>>  data_arrayList, Boolean showQuantity) {
        this.context        = context;
        this.serverdata_Arraylist = data_arrayList;
        mShowQuantity       = showQuantity;
    }

    @Override
    public int getCount() { return serverdata_Arraylist.size(); }

    @Override
    public Object getItem(int position) { return null; }

    @Override
    public long getItemId(int position) { return 0; }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if (convertView == null) {
            gridView = new View(context);
            gridView = inflater.inflate(R.layout.gdv_item_clothing, null);
            resultp = serverdata_Arraylist.get(position);

            ImageView img_cloth_photo = (ImageView) gridView.findViewById(R.id.img_clothing);
            TextView lbl_price = (TextView) gridView.findViewById(R.id.lbl_price);
            TextView lbl_name = (TextView) gridView.findViewById(R.id.lbl_name);

            lbl_name.setText(resultp.get("clothingName"));
            lbl_price.setText(resultp.get("clothingPrice"));
            imageLoader.DisplayImage("http://easygomm.com/" + resultp.get("cloth_photo"), img_cloth_photo);
        } else { gridView = (View) convertView; }
        return gridView;
    }
}
