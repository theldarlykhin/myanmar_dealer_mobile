package com.hnttechs.www.mdbuyer;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.soundcloud.android.crop.Crop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Created by dell on 7/30/16.
 */
public class Create_Product extends ActionBarActivity {

    Uri selectedImageUri;
    String selectedPath;
    private ImageView btn_add_image, preview_image1, preview_image2, preview_image3, preview_image4;
    private Spinner spn_category;
    private Spinner spn_delivery_method;
    final Context context = this;
    final List<String> list = new ArrayList<String>();
    final List<String> list_delivery_method = new ArrayList<String>();
    static TextView txt_title;
    static TextView txt_description;
    static TextView txt_price;
    static TextView txt_color;
    static TextView txt_size;
    static EditText txt_delivery_charges;
    static SharedPreferences mPreferences;
    String User_id;
    static int upload_image_no = 0;

    static String encoded1;
    static String encoded2;
    static String encoded3;
    static String encoded4;
    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_product);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        btn_add_image = (ImageView) findViewById(R.id.btn_add_image);
        preview_image1 = (ImageView) findViewById(R.id.preview_image1);
        preview_image2 = (ImageView) findViewById(R.id.preview_image2);
        preview_image3 = (ImageView) findViewById(R.id.preview_image3);
        preview_image4 = (ImageView) findViewById(R.id.preview_image4);
        spn_category = (Spinner) findViewById(R.id.spn_category);
        spn_delivery_method = (Spinner) findViewById(R.id.spn_delivery_method);
        Button btn_create = (Button) findViewById(R.id.btn_create);
        txt_title = (TextView) findViewById(R.id.txt_title);
        txt_description = (TextView) findViewById(R.id.lbl_description);
        txt_price = (TextView) findViewById(R.id.txt_price);
        txt_color = (TextView) findViewById(R.id.txt_color);
        txt_size = (TextView) findViewById(R.id.txt_size);
        txt_delivery_charges = (EditText) findViewById(R.id.txt_delivery_charges);

        list.add(0, "bath_supplies");
        list.add(1, "beauty_equipments");
        list.add(2, "car_accessories");
        list.add(3, "computers_laptops");
        list.add(4, "eletronic_related");
        list.add(5, "equipments");
        list.add(6, "fashion_related");
        list.add(7, "bags");
        list.add(8, "cloths");
        list.add(9, "footwears");
        list.add(10, "hats");
        list.add(11, "gifts");
        list.add(12, "home_appliance");
        list.add(13, "instruments");
        list.add(14, "machines");
        list.add(15, "comesmetics");
        list.add(16, "medicines");
        list.add(17, "motorcycle");
        list.add(18, "sports");
        list.add(19, "phone_related");
        list.add(20, "toys");


        list_delivery_method.add(0, "Self_delivery");
        list_delivery_method.add(1, "Transporter Logistic");

        mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);
        User_id = mPreferences.getString("UserId", "0");


        ArrayAdapter<String> adp1 = new ArrayAdapter<String>(this,
                R.layout.spinner_item, list);
        adp1.setDropDownViewResource(R.layout.spinner_item);
        spn_category.setAdapter(adp1);

        ArrayAdapter<String> adp_delivery_method = new ArrayAdapter<String>(this,
                R.layout.spinner_item, list_delivery_method);
        adp_delivery_method.setDropDownViewResource(R.layout.spinner_item);
        spn_delivery_method.setAdapter(adp_delivery_method);


        btn_add_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Crop.pickImage(Create_Product.this);


//                final Dialog dialog = new Dialog(context);
//                dialog.setContentView(R.layout.dialog_choose_photo);
//                dialog.setTitle("Title...");
//
//                // set the custom dialog components - text, image and button
//                Button btn_camera = (Button) dialog.findViewById(R.id.btn_camera);
//                btn_camera.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        startActivityForResult(takePicture, 0);//zero can be replaced with any action code
//                        dialog.dismiss();
//                    }
//                });
//                Button btn_gallery = (Button) dialog.findViewById(R.id.btn_gallery);
//                // if button is clicked, close the custom dialog
//                btn_gallery.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {


//                        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
//                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                        startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
//


//                        dialog.dismiss();
//                    }
//                });
//
//                dialog.show();
            }
        });


        btn_create.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (encoded1 == null && encoded1.isEmpty() && encoded1.equals("null")) {
                    encoded1 = "null";
                }
                if (encoded2 == null && encoded2.isEmpty() && encoded2.equals("null")) {
                    encoded2 = "null";
                }
                if (encoded3 == null && encoded3.isEmpty() && encoded3.equals("null")) {
                    encoded3 = "null";
                }
                if (encoded4 == null && encoded4.isEmpty() && encoded4.equals("null")) {
                    encoded4 = "null";
                }

                sendPostRequest(txt_title.getText().toString(), spn_category.getSelectedItem().toString(),
                        txt_description.getText().toString(), txt_price.getText().toString(),
                        spn_delivery_method.getSelectedItem().toString(), txt_color.getText().toString(),
                        txt_size.getText().toString(), encoded1, encoded2, encoded3, encoded4, User_id);

            }
        });

        spn_delivery_method.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (spn_delivery_method.getSelectedItem().toString().equals("Transporter Logistic")) {
                    txt_delivery_charges.setText("2000");
                    txt_delivery_charges.setEnabled(false);
                } else {
                    txt_delivery_charges.setText("");
                    txt_delivery_charges.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {

        if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
            beginCrop(imageReturnedIntent.getData());
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, imageReturnedIntent);
        }


//        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
//        switch(requestCode) {
//            case 0:
//                if(resultCode == RESULT_OK){
//                    Uri selectedImage = imageReturnedIntent.getData();
//                    if(upload_image_no==0) {
//                        preview_image1.setImageURI(selectedImage);
//                    }
//                    if(upload_image_no==1) {
//                        preview_image2.setImageURI(selectedImage);
//                    }
//                    if(upload_image_no==2) {
//                        preview_image3.setImageURI(selectedImage);
//                    }
//                    if(upload_image_no==3) {
//                        preview_image4.setImageURI(selectedImage);
//                    }
//                    upload_image_no++;
//                }
//
//                break;
//            case 1:
//                try {
//                    if (resultCode == RESULT_OK) {
//                        Uri selectedImage = imageReturnedIntent.getData();
//        if (upload_image_no == 0) {
//            preview_image1.setImageURI(selectedImage);
//            Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));
//
//
//            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
//            byte[] byteArray = byteArrayOutputStream.toByteArray();
//
//            encoded1 = Base64.encodeToString(byteArray, Base64.DEFAULT);
//
//        }
//        if (upload_image_no == 1) {
//            preview_image2.setImageURI(selectedImage);
//
//            Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));
//
//
//            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
//            byte[] byteArray = byteArrayOutputStream.toByteArray();
//
//            encoded2 = Base64.encodeToString(byteArray, Base64.DEFAULT);
//
//        }
//        if (upload_image_no == 2) {
//            preview_image3.setImageURI(selectedImage);
//
//            Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));
//
//
//            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
//            byte[] byteArray = byteArrayOutputStream.toByteArray();
//
//            encoded3 = Base64.encodeToString(byteArray, Base64.DEFAULT);
//
//        }
//        if (upload_image_no == 3) {
//            preview_image4.setImageURI(selectedImage);
//
//            Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));
//
//
//            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
//            byte[] byteArray = byteArrayOutputStream.toByteArray();
//
//            encoded4 = Base64.encodeToString(byteArray, Base64.DEFAULT);
//
//        }
//        upload_image_no++;
//                    }
//                }catch (IOException e) {}
//                break;


//        }
    }


    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            try {


                if (upload_image_no == 0) {
                    preview_image1.setImageURI(Crop.getOutput(result));
                    Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(Crop.getOutput(result)));


                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();

                    encoded1 = Base64.encodeToString(byteArray, Base64.DEFAULT);

                }
                if (upload_image_no == 1) {
                    preview_image2.setImageURI(Crop.getOutput(result));

                    Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(Crop.getOutput(result)));


                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();

                    encoded2 = Base64.encodeToString(byteArray, Base64.DEFAULT);

                }
                if (upload_image_no == 2) {
                    preview_image3.setImageURI(Crop.getOutput(result));

                    Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(Crop.getOutput(result)));


                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();

                    encoded3 = Base64.encodeToString(byteArray, Base64.DEFAULT);

                }
                if (upload_image_no == 3) {
                    preview_image4.setImageURI(Crop.getOutput(result));

                    Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(Crop.getOutput(result)));


                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();

                    encoded4 = Base64.encodeToString(byteArray, Base64.DEFAULT);

                }
                upload_image_no++;
            } catch (IOException e) {
            }


        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    private void sendPostRequest(String title, String Category, String Description, String Price,
                                 String DeliveryCategory, String Color, String Size, String image1,
                                 String image2, String image3, String image4, String UserId) {

        class SendPostReqAsyncTask extends AsyncTask<Object, Void, String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(Create_Product.this);
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                mProgressDialog.setMessage("Creating new Product...");
                mProgressDialog.setIndeterminate(false);
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Object... params) {

                String paramTitle = (String) params[0];
                String paramCategory = (String) params[1];
                String paramDescription = (String) params[2];
                String paramPrice = (String) params[3];
                String paramDeliveryCategory = (String) params[4];
                String paramColor = (String) params[5];
                String paramSize = (String) params[6];
                String paramImage1 = (String) params[7];
                String paramImage2 = (String) params[8];
                String paramImage3 = (String) params[9];
                String paramImage4 = (String) params[10];
                String paramUserId = (String) params[11];

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost("http://www.myanmardealer.com/send_to_web");

                BasicNameValuePair titleBasicNameValuePair = new BasicNameValuePair("title", paramTitle);
                BasicNameValuePair categoryBasicNameValuePAir = new BasicNameValuePair("category", paramCategory.toString());
                BasicNameValuePair descriptionBasicNameValuePAir = new BasicNameValuePair("description", paramDescription.toString());
                BasicNameValuePair priceBasicNameValuePair = new BasicNameValuePair("price", paramPrice.toString());
                BasicNameValuePair deliverycategoryBasicNameValuePAir = new BasicNameValuePair("delivery_method", paramDeliveryCategory.toString());
                BasicNameValuePair colorBasicNameValuePair = new BasicNameValuePair("color", paramColor.toString());
                BasicNameValuePair sizeBasicNameValuePAir = new BasicNameValuePair("size", paramSize.toString());
                BasicNameValuePair image1BasicNameValuePAir = new BasicNameValuePair("image1", paramImage1.toString());
                BasicNameValuePair image2BasicNameValuePAir = new BasicNameValuePair("image2", paramImage2.toString());
                BasicNameValuePair image3BasicNameValuePAir = new BasicNameValuePair("image3", paramImage3.toString());
                BasicNameValuePair image4BasicNameValuePAir = new BasicNameValuePair("image4", paramImage4.toString());
                BasicNameValuePair useridBasicNameValuePAir = new BasicNameValuePair("user_id", paramUserId.toString());

                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(titleBasicNameValuePair);
                nameValuePairList.add(categoryBasicNameValuePAir);
                nameValuePairList.add(descriptionBasicNameValuePAir);
                nameValuePairList.add(priceBasicNameValuePair);
                nameValuePairList.add(deliverycategoryBasicNameValuePAir);
                nameValuePairList.add(colorBasicNameValuePair);
                nameValuePairList.add(sizeBasicNameValuePAir);
                nameValuePairList.add(image1BasicNameValuePAir);
                nameValuePairList.add(image2BasicNameValuePAir);
                nameValuePairList.add(image3BasicNameValuePAir);
                nameValuePairList.add(image4BasicNameValuePAir);
                nameValuePairList.add(useridBasicNameValuePAir);

                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);
                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        InputStream inputStream = httpResponse.getEntity().getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;

                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
                        return stringBuilder.toString();
                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                mProgressDialog.dismiss();
                finish();
            }
        }
        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(title, Category, Description, Price, DeliveryCategory, Color,
                Size, image1, image2, image3, image4, UserId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;
            case R.id.action_message:
                Intent message = new Intent(getBaseContext(), Message_Box.class);
                startActivity(message);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
