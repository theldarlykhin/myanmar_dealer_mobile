package com.hnttechs.www.mdbuyer;

/**
 * Created by dell on 8/4/16.
 */

import android.content.Intent;
import android.graphics.drawable.Drawable;

public class CropingOption {
    public CharSequence title;
    public Drawable icon;
    public Intent appIntent;
}