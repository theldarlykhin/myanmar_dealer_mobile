package com.hnttechs.www.mdbuyer;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dell on 8/4/16.
 */
public class Edit_Product_Bath_Supplies extends ActionBarActivity {

    static SharedPreferences mPreferences;
    String User_id;
    String Product_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_product_bath_supplies);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        final EditText txt_title = (EditText) findViewById(R.id.txt_title);
        final EditText txt_ingredient = (EditText) findViewById(R.id.txt_ingredient);
        final EditText txt_usage = (EditText) findViewById(R.id.txt_usage);
        final EditText txt_made_by_country = (EditText) findViewById(R.id.txt_made_by_country);
        final EditText txt_description = (EditText) findViewById(R.id.txt_description);
        final EditText txt_brand = (EditText) findViewById(R.id.txt_brand);
        final EditText txt_effect = (EditText) findViewById(R.id.txt_effect);
        final EditText txt_certification = (EditText) findViewById(R.id.txt_certification);
        final EditText txt_age_group = (EditText) findViewById(R.id.txt_age_group);
        final EditText txt_price = (EditText) findViewById(R.id.txt_price);
        final EditText txt_quantity = (EditText) findViewById(R.id.txt_quantity);
        Button btn_update = (Button) findViewById(R.id.btn_update);


        mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);
        User_id = mPreferences.getString("UserId", "0");


        Intent i = getIntent();
        Product_id = i.getStringExtra("id");


        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendPostRequest(
                        User_id, Product_id,
                        txt_title.getText().toString(),
                        txt_ingredient.getText().toString(),
                        txt_usage.getText().toString(),
                        txt_made_by_country.getText().toString(),
                        txt_description.getText().toString(),
                        txt_brand.getText().toString(),
                        txt_effect.getText().toString(),
                        txt_certification.getText().toString(),
                        txt_age_group.getText().toString(),
                        txt_price.getText().toString(),
                        txt_quantity.getText().toString());
            }
        });


    }


    private void sendPostRequest(String User_id, String Product_id, String title, String ingredient, String usage, String made_by_country,
                                 String description, String brand, String effect, String certification,
                                 String age_group, String price, String quantity) {

        class SendPostReqAsyncTask extends AsyncTask<Object, Void, String> {

            @Override
            protected String doInBackground(Object... params) {

                String paramUserId = (String) params[0];
                String paramProductId = (String) params[1];
                String paramTitle = (String) params[2];
                String paramingredient = (String) params[3];
                String paramusage = (String) params[4];
                String parammade_by_country = (String) params[5];
                String paramdescription = (String) params[6];
                String parambrand = (String) params[7];
                String parameffect = (String) params[8];
                String paramcertification = (String) params[9];
                String paramage_group = (String) params[10];
                String paramprice = (String) params[11];
                String paramquantity = (String) params[12];

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost("http://www.myanmardealer.com/edit_my_product");

                BasicNameValuePair userIdBasicNameValuePair = new BasicNameValuePair("user_id", paramUserId);
                BasicNameValuePair productIdBasicNameValuePAir = new BasicNameValuePair("product_id", paramProductId.toString());
                BasicNameValuePair titleBasicNameValuePair = new BasicNameValuePair("title", paramTitle);
                BasicNameValuePair ingredientBasicNameValuePAir = new BasicNameValuePair("ingredient", paramingredient.toString());
                BasicNameValuePair usageBasicNameValuePAir = new BasicNameValuePair("ingredient", paramusage.toString());
                BasicNameValuePair made_by_countryBasicNameValuePAir = new BasicNameValuePair("made_by_country", parammade_by_country.toString());
                BasicNameValuePair descriptionBasicNameValuePAir = new BasicNameValuePair("description", paramdescription.toString());
                BasicNameValuePair brandBasicNameValuePAir = new BasicNameValuePair("brand", parambrand.toString());
                BasicNameValuePair effectBasicNameValuePAir = new BasicNameValuePair("effect", parameffect.toString());
                BasicNameValuePair certificationBasicNameValuePAir = new BasicNameValuePair("certification", paramcertification.toString());
                BasicNameValuePair age_groupBasicNameValuePAir = new BasicNameValuePair("age_group", paramage_group.toString());
                BasicNameValuePair priceBasicNameValuePAir = new BasicNameValuePair("price", paramprice.toString());
                BasicNameValuePair quantityBasicNameValuePAir = new BasicNameValuePair("quantity", paramquantity.toString());


                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(userIdBasicNameValuePair);
                nameValuePairList.add(productIdBasicNameValuePAir);
                nameValuePairList.add(titleBasicNameValuePair);
                nameValuePairList.add(ingredientBasicNameValuePAir);
                nameValuePairList.add(usageBasicNameValuePAir);
                nameValuePairList.add(made_by_countryBasicNameValuePAir);
                nameValuePairList.add(descriptionBasicNameValuePAir);
                nameValuePairList.add(brandBasicNameValuePAir);
                nameValuePairList.add(effectBasicNameValuePAir);
                nameValuePairList.add(certificationBasicNameValuePAir);
                nameValuePairList.add(age_groupBasicNameValuePAir);
                nameValuePairList.add(priceBasicNameValuePAir);
                nameValuePairList.add(quantityBasicNameValuePAir);

                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);
                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        InputStream inputStream = httpResponse.getEntity().getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;

                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
                        return stringBuilder.toString();
                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
//                dialog.dismiss();
                finish();
            }
        }
        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(User_id, Product_id, title, ingredient, usage, made_by_country,
                 description,  brand,  effect,  certification, age_group,  price, quantity);
    }

}
