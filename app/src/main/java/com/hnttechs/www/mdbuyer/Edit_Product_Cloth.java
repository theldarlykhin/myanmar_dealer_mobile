package com.hnttechs.www.mdbuyer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dell on 8/4/16.
 */
public class Edit_Product_Cloth extends ActionBarActivity {

    static SharedPreferences mPreferences;
    String User_id;
    String Product_id;
    ProgressDialog mProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_product_cloth);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        final EditText txt_title = (EditText) findViewById(R.id.txt_title);
        final EditText txt_quantity = (EditText) findViewById(R.id.txt_quantity);
        final EditText txt_size = (EditText) findViewById(R.id.txt_size);
        final EditText txt_made_by_country = (EditText) findViewById(R.id.txt_made_by_country);
        final EditText txt_made_with = (EditText) findViewById(R.id.txt_made_with);
        final EditText txt_description = (EditText) findViewById(R.id.txt_description);
        final EditText txt_brand = (EditText) findViewById(R.id.txt_brand);
        final EditText txt_gender = (EditText) findViewById(R.id.txt_gender);
        final EditText txt_color = (EditText) findViewById(R.id.txt_color);
        final EditText txt_price = (EditText) findViewById(R.id.txt_price);
        Button btn_update = (Button) findViewById(R.id.btn_update);


        mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);
        User_id = mPreferences.getString("UserId", "0");


        Intent i = getIntent();
        Product_id = i.getStringExtra("id");


        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendPostRequest(
                        User_id, Product_id,
                        txt_title.getText().toString(),
                        txt_quantity.getText().toString(),
                        txt_size.getText().toString(),
                        txt_made_by_country.getText().toString(),
                        txt_made_with.getText().toString(),
                        txt_description.getText().toString(),
                        txt_brand.getText().toString(),
                        txt_gender.getText().toString(),
                        txt_color.getText().toString(),
                        txt_price.getText().toString());
            }
        });


    }


    private void sendPostRequest(
        String UserId,
        String ProductId,
        String Title,
        String quantity,
        String size,
        String made_by_country,
        String made_with,
        String description,
        String brand,
        String gender,
        String color,
        String price) {


        class SendPostReqAsyncTask extends AsyncTask<Object, Void, String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(Edit_Product_Cloth.this);
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                mProgressDialog.setMessage("Updating Product...");
                mProgressDialog.setIndeterminate(false);
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Object... params) {

                String paramUserId = (String) params[0];
                String paramProductId = (String) params[1];
                String paramTitle = (String) params[2];
                String paramquantity = (String) params[3];
                String paramsize = (String) params[4];
                String parammade_by_country = (String) params[5];
                String parammade_with = (String) params[6];
                String paramdescription = (String) params[7];
                String parambrand = (String) params[8];
                String paramgender = (String) params[9];
                String paramcolor = (String) params[10];
                String paramprice = (String) params[11];

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost("http://www.myanmardealer.com/edit_my_product");

                BasicNameValuePair userIdBasicNameValuePair = new BasicNameValuePair("user_id", paramUserId);
                BasicNameValuePair productIdBasicNameValuePAir = new BasicNameValuePair("product_id", paramProductId.toString());
                BasicNameValuePair titleBasicNameValuePair = new BasicNameValuePair("title", paramTitle);
                BasicNameValuePair quantityBasicNameValuePAir = new BasicNameValuePair("quantity", paramquantity.toString());
                BasicNameValuePair sizeBasicNameValuePAir = new BasicNameValuePair("size", paramsize.toString());
                BasicNameValuePair made_by_countryBasicNameValuePAir = new BasicNameValuePair("made_by_country", parammade_by_country.toString());
                BasicNameValuePair made_withBasicNameValuePAir = new BasicNameValuePair("made_with", parammade_with.toString());
                BasicNameValuePair descriptionBasicNameValuePAir = new BasicNameValuePair("description", paramdescription.toString());
                BasicNameValuePair brandBasicNameValuePAir = new BasicNameValuePair("brand", parambrand.toString());
                BasicNameValuePair genderBasicNameValuePAir = new BasicNameValuePair("gender", paramgender.toString());
                BasicNameValuePair colorBasicNameValuePAir = new BasicNameValuePair("color", paramcolor.toString());
                BasicNameValuePair priceBasicNameValuePAir = new BasicNameValuePair("price", paramprice.toString());


                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(userIdBasicNameValuePair);
                nameValuePairList.add(productIdBasicNameValuePAir);
                nameValuePairList.add(titleBasicNameValuePair);
                nameValuePairList.add(quantityBasicNameValuePAir);
                nameValuePairList.add(sizeBasicNameValuePAir);
                nameValuePairList.add(made_by_countryBasicNameValuePAir);
                nameValuePairList.add(made_withBasicNameValuePAir);
                nameValuePairList.add(descriptionBasicNameValuePAir);
                nameValuePairList.add(brandBasicNameValuePAir);
                nameValuePairList.add(genderBasicNameValuePAir);
                nameValuePairList.add(colorBasicNameValuePAir);
                nameValuePairList.add(priceBasicNameValuePAir);

                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);
                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        InputStream inputStream = httpResponse.getEntity().getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;

                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
                        return stringBuilder.toString();
                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                mProgressDialog.dismiss();
                finish();
            }
        }
        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(User_id, Product_id, Title, quantity, size, made_by_country,
                 made_with,  description,  brand,  gender, color,  price);
    }

}
