package com.hnttechs.www.mdbuyer;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dell on 8/4/16.
 */
public class Edit_Store extends ActionBarActivity {

    static SharedPreferences mPreferences;
    String User_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_store);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);
        User_id = mPreferences.getString("UserId", "0");


        final EditText txt_shop_name = (EditText) findViewById(R.id.txt_shop_name);
        final EditText txt_shop_address = (EditText) findViewById(R.id.txt_shop_address);
        final EditText txt_shop_contact = (EditText) findViewById(R.id.txt_shop_contact);
        final EditText txt_description = (EditText) findViewById(R.id.txt_description);
        final EditText txt_term = (EditText) findViewById(R.id.txt_term);
        Button btn_update = (Button)findViewById(R.id.btn_update);

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendPostRequest(
                        User_id,
                        txt_shop_name.getText().toString(),
                        txt_shop_address.getText().toString(),
                        txt_shop_contact.getText().toString(),
                        txt_description.getText().toString(),
                        txt_term.getText().toString());
            }
        });
    }




    private void sendPostRequest(String User_id, String store_name, String store_address, String store_contact,
                                 String description, String term) {

        class SendPostReqAsyncTask extends AsyncTask<Object, Void, String> {

            @Override
            protected String doInBackground(Object... params) {

                String paramUser_id = (String) params[0];
                String paramstore_name = (String) params[1];
                String paramstore_address = (String) params[2];
                String paramstore_contact = (String) params[3];
                String paramdescription = (String) params[4];
                String paramterm = (String) params[5];


                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost("http://www.myanmardealer.com/edit_my_store");

                BasicNameValuePair User_idBasicNameValuePair = new BasicNameValuePair("user_id", paramUser_id);
                BasicNameValuePair store_nameBasicNameValuePair = new BasicNameValuePair("store_name", paramstore_name);
                BasicNameValuePair store_addressBasicNameValuePair = new BasicNameValuePair("store_address", paramstore_address);
                BasicNameValuePair store_contactBasicNameValuePair = new BasicNameValuePair("store_contact", paramstore_contact);
                BasicNameValuePair descriptionBasicNameValuePair = new BasicNameValuePair("description", paramdescription);
                BasicNameValuePair termBasicNameValuePair = new BasicNameValuePair("term", paramterm);



                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();



                nameValuePairList.add(User_idBasicNameValuePair);
                nameValuePairList.add(store_nameBasicNameValuePair);
                nameValuePairList.add(store_addressBasicNameValuePair);
                nameValuePairList.add(store_contactBasicNameValuePair);
                nameValuePairList.add(descriptionBasicNameValuePair);
                nameValuePairList.add(termBasicNameValuePair);


                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);
                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        InputStream inputStream = httpResponse.getEntity().getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;

                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
                        return stringBuilder.toString();
                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
//                dialog.dismiss();
                finish();
            }
        }
        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(User_id, store_name, store_address, store_contact, description, term);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;
            case R.id.action_message:
                Intent message = new Intent(getBaseContext(),Message_Box.class);
                startActivity(message);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
