package com.hnttechs.www.mdbuyer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by dell on 7/15/15.
 */
public class Footwear_GridViewAdapter extends BaseAdapter {

    private Context context;
    private final String[] gridValues;

    public Footwear_GridViewAdapter(Context context, String[] gridValues) {
        this.context        = context;
        this.gridValues     = gridValues;
    }

    @Override
    public int getCount() { return gridValues.length; }

    @Override
    public Object getItem(int position) { return null; }

    @Override
    public long getItemId(int position) { return 0; }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if (convertView == null) {
            gridView = new View(context);
            gridView = inflater.inflate(R.layout.gdv_item_clothing, null);

            ImageView img_car = (ImageView) gridView.findViewById(R.id.img_clothing);
            TextView lbl_made = (TextView) gridView
                    .findViewById(R.id.lbl_price);

            lbl_made.setText(gridValues[position]);
            if(gridValues[position].toString().equals("25,000 Kyats")) {img_car.setImageResource(R.drawable.shoe3);}
            else if(gridValues[position].toString().equals("10,000 Kyats")) {img_car.setImageResource(R.drawable.shoe4);}
            else if(gridValues[position].toString().equals("30,000 Kyats")) {img_car.setImageResource(R.drawable.shoe1);}
            else if(gridValues[position].toString().equals("23,000 Kyats")) {img_car.setImageResource(R.drawable.shoe2);}
            else if(gridValues[position].toString().equals("3,000 Kyats")) {img_car.setImageResource(R.drawable.shoe3);}
            else if(gridValues[position].toString().equals("2,000 Kyats")) {img_car.setImageResource(R.drawable.shoe1);}

        } else { gridView = (View) convertView; }

        return gridView;
    }
}
