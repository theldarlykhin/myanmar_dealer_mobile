package com.hnttechs.www.mdbuyer;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.*;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hnttechs.www.mdbuyer.model.NavDrawerItem;

/**
 * Created by dell on 2/28/16.
 */
public class HomeFragment extends Fragment {
    public HomeFragment(){}
    static ViewPager viewPager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_home, container, false);

        TabLayout tabLayout = (TabLayout) rootview.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.home_icon));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.big_deal_icon));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.shopping_cart));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.profile));

        viewPager = (ViewPager) rootview.findViewById(R.id.view_pager);
        viewPager.setAdapter(new PagerAdapter
                (getFragmentManager(), tabLayout.getTabCount()));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return rootview;
    }

    public class PagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    Tab_Home tab_home = new Tab_Home();
                    return tab_home;
                case 1:
                    Tab_BigDeal tab_bigdeal = new Tab_BigDeal();
                    return tab_bigdeal;
                case 2:
                    Tab_ShoppingCartFragment tab_shoppingcart = new Tab_ShoppingCartFragment();
                    return tab_shoppingcart;
                case 3:
//                    if (MainActivity.mPreferences.contains("AuthToken")) {
                        Tab_Dashboard tab_dashboard= new Tab_Dashboard();
                        return tab_dashboard;
//                    }else {
//                        Intent login = new Intent(getActivity().getBaseContext(), LogInFragment.class);
//                        startActivity(login);
//                    }
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }
}