package com.hnttechs.www.mdbuyer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dell on 8/7/16.
 */
public class New_Store extends ActionBarActivity {

    static String encoded1;
    static String encoded2;
    static String encoded3;
    static String encoded4;
    static String encoded5;
    static String encoded6;
    static String encoded7;


    private ImageView description_image1;
    private ImageView description_image2;
    private ImageView description_image3;
    private ImageView description_image4;
    private ImageView ads_one;
    private ImageView ads_two;
    private ImageView avatar;
    int type = 0;
    static SharedPreferences mPreferences;
    String User_id;
    String User_name;
    ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_store);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);


        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        final EditText term = (EditText) findViewById(R.id.txt_term);
        final EditText store_name = (EditText) findViewById(R.id.txt_store_name);
        final EditText store_address = (EditText) findViewById(R.id.txt_store_address);
        final EditText store_contact = (EditText) findViewById(R.id.txt_store_contact);
        final EditText description = (EditText) findViewById(R.id.txt_description);
        Button btn_open_store = (Button) findViewById(R.id.btn_open_store);


        Button btn_description_image1 = (Button) findViewById(R.id.btn_description_image1);
        Button btn_description_image2 = (Button) findViewById(R.id.btn_description_image2);
        Button btn_description_image3 = (Button) findViewById(R.id.btn_description_image3);
        Button btn_description_image4 = (Button) findViewById(R.id.btn_description_image4);
        Button btn_ads_one = (Button) findViewById(R.id.btn_ads_one);
        Button btn_ads_two = (Button) findViewById(R.id.btn_ads_two);
        Button btn_avatar = (Button) findViewById(R.id.btn_avatar);

        ads_one = (ImageView) findViewById(R.id.img_ads_one);
        ads_two = (ImageView) findViewById(R.id.img_ads_two);
        avatar = (ImageView) findViewById(R.id.img_avatar);
        description_image1 = (ImageView) findViewById(R.id.img_description_image1);
        description_image2 = (ImageView) findViewById(R.id.img_description_image2);
        description_image3 = (ImageView) findViewById(R.id.img_description_image3);
        description_image4 = (ImageView) findViewById(R.id.img_description_image4);

        mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);
        User_id = mPreferences.getString("UserId", "0");
        User_name = mPreferences.getString("UserName", "0");

        btn_description_image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = 1;
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
            }
        });
        btn_description_image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = 2;
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
            }
        });
        btn_description_image3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = 3;
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
            }
        });
        btn_description_image4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = 4;
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
            }
        });
        btn_ads_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = 5;
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
            }
        });
        btn_ads_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = 6;
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
            }
        });
        btn_avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = 7;
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
            }
        });

        btn_open_store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendPostRequest(encoded1,
                        encoded2,
                        encoded3,
                        encoded4,
                        term.getText().toString(),
                        encoded5,
                        encoded6,
                        User_id,
                        encoded7,
                        store_name.getText().toString(),
                        store_address.getText().toString(),
                        store_contact.getText().toString(),
                        description.getText().toString(),
                        User_name);

            }
        });


    }

    private void sendPostRequest(
            String description_image1,
            String description_image2,
            String description_image3,
            String description_image4,
            String term,
            String ads_one,
            String ads_two,
            String user_id,
            String avatar,
            String store_name,
            String store_address,
            String store_contact,
            String description,
            String seller_name) {


        class SendPostReqAsyncTask extends AsyncTask<Object, Void, String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(New_Store.this);
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                mProgressDialog.setMessage("Creating new Store...");
                mProgressDialog.setIndeterminate(false);
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Object... params) {

                String paramdescription_image1 = (String) params[0];
                String paramdescription_image2 = (String) params[1];
                String paramdescription_image3 = (String) params[2];
                String paramdescription_image4 = (String) params[3];
                String paramterm = (String) params[4];
                String paramads_one = (String) params[5];
                String paramads_two = (String) params[6];
                String paramuser_id = (String) params[7];
                String paramavatar = (String) params[8];
                String paramstore_name = (String) params[9];
                String paramstore_address = (String) params[10];
                String paramstore_contact = (String) params[11];
                String paramdescription = (String) params[12];
                String paramseller_name = (String) params[13];


                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost("http://www.myanmardealer.com/open_new_store");


                BasicNameValuePair description_image1BasicNameValuePair = new BasicNameValuePair("description_image1", paramdescription_image1);
                BasicNameValuePair description_image2BasicNameValuePair = new BasicNameValuePair("description_image2", paramdescription_image2);
                BasicNameValuePair description_image3BasicNameValuePair = new BasicNameValuePair("description_image3", paramdescription_image3);
                BasicNameValuePair description_image4BasicNameValuePair = new BasicNameValuePair("description_image4", paramdescription_image4);
                BasicNameValuePair termBasicNameValuePair = new BasicNameValuePair("term", paramterm);
                BasicNameValuePair ads_oneBasicNameValuePair = new BasicNameValuePair("ads_one", paramads_one);
                BasicNameValuePair ads_twoBasicNameValuePair = new BasicNameValuePair("ads_two", paramads_two);
                BasicNameValuePair user_idBasicNameValuePair = new BasicNameValuePair("user_id", paramuser_id);
                BasicNameValuePair avatarBasicNameValuePair = new BasicNameValuePair("avatar", paramavatar);
                BasicNameValuePair store_nameBasicNameValuePair = new BasicNameValuePair("store_name", paramstore_name);
                BasicNameValuePair store_addressBasicNameValuePair = new BasicNameValuePair("store_address", paramstore_address);
                BasicNameValuePair store_contactBasicNameValuePair = new BasicNameValuePair("store_contact", paramstore_contact);
                BasicNameValuePair descriptionBasicNameValuePair = new BasicNameValuePair("description", paramdescription);
                BasicNameValuePair seller_nameBasicNameValuePair = new BasicNameValuePair("seller_name", paramseller_name);


                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(description_image1BasicNameValuePair);
                nameValuePairList.add(description_image2BasicNameValuePair);
                nameValuePairList.add(description_image3BasicNameValuePair);
                nameValuePairList.add(description_image4BasicNameValuePair);
                nameValuePairList.add(termBasicNameValuePair);
                nameValuePairList.add(ads_oneBasicNameValuePair);
                nameValuePairList.add(ads_twoBasicNameValuePair);
                nameValuePairList.add(user_idBasicNameValuePair);
                nameValuePairList.add(avatarBasicNameValuePair);
                nameValuePairList.add(store_nameBasicNameValuePair);
                nameValuePairList.add(store_addressBasicNameValuePair);
                nameValuePairList.add(store_contactBasicNameValuePair);
                nameValuePairList.add(descriptionBasicNameValuePair);
                nameValuePairList.add(seller_nameBasicNameValuePair);


                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);
                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        InputStream inputStream = httpResponse.getEntity().getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;

                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
                        return stringBuilder.toString();
                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                mProgressDialog.dismiss();
                Tab_Dashboard.layout_create_store.setVisibility(View.GONE);
                Tab_Dashboard.layout_create_product.setVisibility(View.VISIBLE);
                SharedPreferences.Editor editor = mPreferences.edit();
                editor.putString("Has_Store", "yes");
                editor.commit();
                finish();
            }
        }
        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(description_image1
                , description_image2
                , description_image3
                , description_image4
                , term
                , ads_one
                , ads_two
                , user_id
                , avatar
                , store_name
                , store_address
                , store_contact
                , description
                , seller_name);

    }


    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
//            case 0:
//                if(resultCode == RESULT_OK){
//                    Uri selectedImage = imageReturnedIntent.getData();
//                    if(upload_image_no==0) {
//                        preview_image1.setImageURI(selectedImage);
//                    }
//                    if(upload_image_no==1) {
//                        preview_image2.setImageURI(selectedImage);
//                    }
//                    if(upload_image_no==2) {
//                        preview_image3.setImageURI(selectedImage);
//                    }
//                    if(upload_image_no==3) {
//                        preview_image4.setImageURI(selectedImage);
//                    }
//                    upload_image_no++;
//                }
//
//                break;
            case 1:
                try {
                    if (resultCode == RESULT_OK) {
                        Uri selectedImage = imageReturnedIntent.getData();
                        if (type == 1) {
                            description_image1.setImageURI(selectedImage);
                            Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));


                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                            byte[] byteArray = byteArrayOutputStream.toByteArray();

                            encoded1 = "data:image/png;base64," + Base64.encodeToString(byteArray, Base64.DEFAULT);

                        }
                        if (type == 2) {
                            description_image2.setImageURI(selectedImage);

                            Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));


                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                            byte[] byteArray = byteArrayOutputStream.toByteArray();

                            encoded2 = "data:image/png;base64," + Base64.encodeToString(byteArray, Base64.DEFAULT);

                        }
                        if (type == 3) {
                            description_image3.setImageURI(selectedImage);

                            Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));


                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                            byte[] byteArray = byteArrayOutputStream.toByteArray();

                            encoded3 = "data:image/png;base64," + Base64.encodeToString(byteArray, Base64.DEFAULT);

                        }
                        if (type == 4) {
                            description_image4.setImageURI(selectedImage);

                            Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));


                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                            byte[] byteArray = byteArrayOutputStream.toByteArray();

                            encoded4 = "data:image/png;base64," + Base64.encodeToString(byteArray, Base64.DEFAULT);

                        }

                        if (type == 5) {
                            ads_one.setImageURI(selectedImage);

                            Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));


                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                            byte[] byteArray = byteArrayOutputStream.toByteArray();

                            encoded5 = "data:image/png;base64," + Base64.encodeToString(byteArray, Base64.DEFAULT);

                        }
                        if (type == 6) {
                            ads_two.setImageURI(selectedImage);

                            Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));


                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                            byte[] byteArray = byteArrayOutputStream.toByteArray();

                            encoded6 = "data:image/png;base64," + Base64.encodeToString(byteArray, Base64.DEFAULT);

                        }

                        if (type == 7) {
                            avatar.setImageURI(selectedImage);

                            Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));


                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                            byte[] byteArray = byteArrayOutputStream.toByteArray();

                            encoded7 = "data:image/png;base64," + Base64.encodeToString(byteArray, Base64.DEFAULT);

                        }
                    }
                } catch (IOException e) {
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;
            case R.id.action_message:
                Intent message = new Intent(getBaseContext(),Message_Box.class);
                startActivity(message);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
