package com.hnttechs.www.mdbuyer;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.pusher.client.channel.User;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 8/3/16.
 */
public class Order_List extends ActionBarActivity {
    static ListView lv_order_list;
    order_list_ListAdapter adapter;

    static String serverData;
    ArrayList<HashMap<String, String>> arraylist;
    ArrayList<HashMap<String, String>> arraylist_line_items;

    static SharedPreferences mPreferences;
    String User_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        lv_order_list = (ListView) findViewById(R.id.lv_order_list);
        mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);
        User_id = mPreferences.getString("UserId", "0");

        new DataFetcherTask().execute();

    }


    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            serverData = null;

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://www.myanmardealer.com/my_product_order_list.txt?user_id=" + User_id);
            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                serverData = EntityUtils.toString(httpEntity);
                Log.d("response", serverData);

                JSONObject jsonObject = new JSONObject(serverData);
                JSONArray jsonArray = jsonObject.getJSONArray("Orders");
                JSONArray jsonArray_lineitems = jsonObject.getJSONArray("LineItems");
                arraylist = new ArrayList<HashMap<String, String>>();

                for (int i = 0; i < jsonArray.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    JSONObject jsonObjectClothing = jsonArray.getJSONObject(i);
                    JSONObject jsonObject_lineitems = jsonArray_lineitems.getJSONObject(i);

                    map.put("customer_name", jsonObjectClothing.getString("customer_name"));
                    map.put("customer_email", jsonObjectClothing.getString("customer_email"));
                    map.put("customer_phone",  jsonObjectClothing.getString("customer_phone"));
                    map.put("customer_city", jsonObjectClothing.getString("customer_city"));
                    map.put("customer_township", jsonObjectClothing.getString("customer_township"));
                    map.put("customer_address", jsonObjectClothing.getString("customer_address"));
                    map.put("cart_id", jsonObjectClothing.getString("cart_id"));


                    map.put("product_name", jsonObject_lineitems.getString("product_name"));
                    map.put("quantity", jsonObject_lineitems.getString("quantity"));
                    map.put("cart_id", jsonObject_lineitems.getString("cart_id"));

                    arraylist.add(map);
                }

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (arraylist != null) {
                adapter = new order_list_ListAdapter(getBaseContext(), arraylist);
                lv_order_list.setAdapter(adapter);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;
            case R.id.action_message:
                Intent message = new Intent(getBaseContext(),Message_Box.class);
                startActivity(message);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
