package com.hnttechs.www.mdbuyer;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 8/3/16.
 */
public class Order_receiveList extends ActionBarActivity {
    static ListView lv_order_receivelist;
    static SharedPreferences mPreferences;
    String User_id;
    order_list_ListAdapter adapter;

    static String serverData;
    ArrayList<HashMap<String, String>> arraylist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_receivelist);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        lv_order_receivelist = (ListView) findViewById(R.id.lv_order_receivelist);

        mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);
        User_id = mPreferences.getString("UserId", "0");

        new DataFetcherTask().execute();


    }


    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            serverData = null;

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://www.myanmardealer.com/order_completed_list?user_id=" + User_id);
            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                serverData = EntityUtils.toString(httpEntity);
                Log.d("response", serverData);

                JSONObject jsonObject = new JSONObject(serverData);
                JSONArray jsonArray = jsonObject.getJSONArray("Orders");
                arraylist = new ArrayList<HashMap<String, String>>();

                for (int i = 0; i < jsonArray.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    JSONObject jsonObjectClothing = jsonArray.getJSONObject(i);

                    map.put("clothingName", jsonObjectClothing.getString("title"));
                    map.put("clothingPrice", jsonObjectClothing.getString("price"));
                    map.put("clothingQuantity", "5");
                    map.put("clothingStore_name", jsonObjectClothing.getString("store_name"));
                    map.put("size", jsonObjectClothing.getString("size"));
                    map.put("color", jsonObjectClothing.getString("color"));
                    map.put("description", jsonObjectClothing.getString("description"));
                    map.put("made_by_country", jsonObjectClothing.getString("made_by_country"));
                    map.put("brand", jsonObjectClothing.getString("brand"));
                    map.put("cloth_photo", jsonObjectClothing.getString("avatar1_file_name"));
                    map.put("user_id", jsonObjectClothing.getString("user_id"));

                    arraylist.add(map);


                }

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (arraylist != null) {

                adapter = new order_list_ListAdapter(getBaseContext(), arraylist);
                lv_order_receivelist.setAdapter(adapter);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;
            case R.id.action_message:
                Intent message = new Intent(getBaseContext(),Message_Box.class);
                startActivity(message);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
