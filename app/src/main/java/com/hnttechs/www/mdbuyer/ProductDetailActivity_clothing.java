package com.hnttechs.www.mdbuyer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.hnttechs.www.mdbuyer.model.Clothing;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dell on 3/8/16.
 */
public class ProductDetailActivity_clothing extends ActionBarActivity {

    String productType;
    static int position;
    static Clothing clothing1;
    ArrayList<HashMap<String, String>> cartData;
    private SharedPreferences mPreferences;
    private Spinner spn_size, spn_color;
    private EditText txt_qty;
    static ArrayList<HashMap<String, String>> serverdata_Arraylist;
    ArrayList<HashMap<String, String>> trim_serverdata_Arraylist;
    static HashMap<String, String> resultp = new HashMap<String, String>();
    GridView gdv_related_item;
    BigDeal_GridViewAdapter adapter;
    static String seller_id;
    static String product_id;
    static String category;

    static String image_string1;
    static String image_string2;
    static String image_string3;
    static String image_string4;
    static TextView txt_shop_name;
    static String serverData;
    static String shop_licence_file_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_detail);

        mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        Intent i = getIntent();
        position = i.getIntExtra("Position", 0);
        category = i.getStringExtra("category");
        serverdata_Arraylist = ClothingActivity.arraylist;

        ImageView img_product_pic = (ImageView) findViewById(R.id.img_product_pic);
        ImageView img_product_pic2 = (ImageView) findViewById(R.id.img_product_pic2);
        ImageView img_product_pic3 = (ImageView) findViewById(R.id.img_product_pic3);
        ImageView img_product_pic4 = (ImageView) findViewById(R.id.img_product_pic4);
        final ImageView img_product_big = (ImageView) findViewById(R.id.img_product_big);
        final ImageView img_back = (ImageView) findViewById(R.id.img_back);
        final ImageView img_shop_icon = (ImageView) findViewById(R.id.shop_icon);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        gdv_related_item = (GridView) findViewById(R.id.gdv_related_item);
        spn_size = (Spinner) findViewById(R.id.spn_size);
        spn_color = (Spinner) findViewById(R.id.spn_color);
        txt_qty = (EditText) findViewById(R.id.txt_qty);
        TextView lbl_price = (TextView) findViewById(R.id.lbl_price);
        TextView lbl_name = (TextView) findViewById(R.id.lbl_name);
        txt_shop_name = (TextView) findViewById(R.id.txt_shop_name);
        Button btn_add_to_cart = (Button) findViewById(R.id.btn_add_to_cart);
        Button btn_buy_now = (Button) findViewById(R.id.btn_buy_now);
        TextView btn_chat = (TextView) findViewById(R.id.btn_chat);
        ImageView btn_share = (ImageView) findViewById(R.id.btn_share);
        Button btn_visit_shop = (Button) findViewById(R.id.btn_visit_shop);
        Button btn_show_license = (Button) findViewById(R.id.btn_show_license);



        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Description"));
        tabLayout.addTab(tabLayout.newTab().setText("Specification"));
        tabLayout.addTab(tabLayout.newTab().setText("Item Rating"));

        final ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        viewPager.setAdapter(new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount()));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        trim_serverdata_Arraylist = serverdata_Arraylist;
        resultp = trim_serverdata_Arraylist.get(position);

        txt_shop_name.setText(resultp.get("clothingStore_name"));
//
//        if (resultp.get("avatar1_edit") != null && !resultp.get("avatar1_edit").isEmpty() && !resultp.get("avatar1_edit").equals("null")) {
//            image_string1 = resultp.get("avatar1_edit").toString();
//        }
//
//        if (resultp.get("avatar2_edit") != null && !resultp.get("avatar2_edit").isEmpty() && !resultp.get("avatar2_edit").equals("null")) {
//            image_string2 = resultp.get("avatar2_edit").toString();
//        }
//
//        if (resultp.get("avatar3_edit") != null && !resultp.get("avatar3_edit").isEmpty() && !resultp.get("avatar3_edit").equals("null")) {
//            image_string3 = resultp.get("avatar3_edit").toString();
//        }
//
//        if (resultp.get("avatar4_edit") != null && !resultp.get("avatar4_edit").isEmpty() && !resultp.get("avatar4_edit").equals("null")) {
//            image_string4 = resultp.get("avatar4_edit").toString();
//        }

        product_id = resultp.get("id");


        btn_buy_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), Activity_Customer_Info.class);
                i.putExtra("product_id",resultp.get("id"));
                i.putExtra("qty",txt_qty.getText().toString());
                startActivity(i);
            }
        });


        btn_visit_shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent visit_shop = new Intent(getBaseContext(), Activity_Shop.class);
                visit_shop.putExtra("store_name", resultp.get("clothingStore_name"));
                startActivity(visit_shop);
            }
        });


//        if (image_string1 != null && !image_string1.isEmpty() && !image_string1.equals("null")) {
//            String base64_string1 = image_string1.substring(22, image_string1.length());
//            byte[] decodedString1 = Base64.decode(base64_string1, Base64.URL_SAFE);
//            img_product_pic.setImageBitmap(BitmapFactory.decodeByteArray(decodedString1, 0, decodedString1.length));
//        }
//        if (image_string2 != null && !image_string2.isEmpty() && !image_string2.equals("null")) {
//
//            String base64_string2 = image_string2.substring(22, image_string2.length());
//            byte[] decodedString2 = Base64.decode(base64_string2, Base64.URL_SAFE);
//            img_product_pic2.setImageBitmap(BitmapFactory.decodeByteArray(decodedString2, 0, decodedString2.length));
//        }
//        if (image_string3 != null && !image_string3.isEmpty() && !image_string3.equals("null")) {
//            String base64_string3 = image_string3.substring(22, image_string3.length());
//            byte[] decodedString3 = Base64.decode(base64_string3, Base64.URL_SAFE);
//            img_product_pic3.setImageBitmap(BitmapFactory.decodeByteArray(decodedString3, 0, decodedString3.length));
//        }
//        if (image_string4 != null && !image_string4.isEmpty() && !image_string4.equals("null")) {
//            String base64_string4 = image_string4.substring(22, image_string4.length());
//            byte[] decodedString4 = Base64.decode(base64_string4, Base64.URL_SAFE);
//            img_product_pic4.setImageBitmap(BitmapFactory.decodeByteArray(decodedString4, 0, decodedString4.length));
//        }
//
//        img_product_pic.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (image_string1 != null && !image_string1.isEmpty() && !image_string1.equals("null")) {
//                    String base64_string1 = image_string1.substring(22, image_string1.length());
//                    byte[] decodedString1 = Base64.decode(base64_string1, Base64.URL_SAFE);
//                    img_product_big.setImageBitmap(BitmapFactory.decodeByteArray(decodedString1, 0, decodedString1.length));
//                }
//            }
//        });
//
//
//        img_product_pic2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (image_string2 != null && !image_string2.isEmpty() && !image_string2.equals("null")) {
//                    String base64_string2 = image_string2.substring(22, image_string2.length());
//                    byte[] decodedString2 = Base64.decode(base64_string2, Base64.DEFAULT);
//                    img_product_big.setImageBitmap(BitmapFactory.decodeByteArray(decodedString2, 0, decodedString2.length));
//                }
//            }
//        });
//
//
//        img_product_pic3.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (image_string3 != null && !image_string3.isEmpty() && !image_string3.equals("null")) {
//                    String base64_string3 = image_string3.substring(22, image_string3.length());
//                    byte[] decodedString3 = Base64.decode(base64_string3, Base64.DEFAULT);
//                    img_product_big.setImageBitmap(BitmapFactory.decodeByteArray(decodedString3, 0, decodedString3.length));
//                }
//            }
//        });
//
//
//        img_product_pic4.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (image_string4 != null && !image_string4.isEmpty() && !image_string4.equals("null")) {
//                    String base64_string4 = image_string4.substring(22, image_string4.length());
//                    byte[] decodedString4 = Base64.decode(base64_string4, Base64.DEFAULT);
//                    img_product_big.setImageBitmap(BitmapFactory.decodeByteArray(decodedString4, 0, decodedString4.length));
//                }
//            }
//        });

        adapter = new BigDeal_GridViewAdapter(getBaseContext(), serverdata_Arraylist, false);
        gdv_related_item.setAdapter(adapter);

        String size = resultp.get("size");
        List<String> size_list = Arrays.asList(size.split(","));

        ArrayAdapter<String> size_dataAdapter = new ArrayAdapter<String>(this,
                R.layout.custom_spinner_item, size_list);
        size_dataAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
        spn_size.setAdapter(size_dataAdapter);


        String color = resultp.get("color");
        List<String> color_list = Arrays.asList(color.split(","));

        ArrayAdapter<String> color_dataAdapter = new ArrayAdapter<String>(this,
                R.layout.custom_spinner_item, color_list);
        color_dataAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
        spn_color.setAdapter(color_dataAdapter);


        lbl_price.setText("" + resultp.get("clothingPrice") + "");
        lbl_name.setText("" + resultp.get("clothingName") + "");


        btn_show_license.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DataFetcherTask().execute();
                AlertDialog.Builder builder = new AlertDialog.Builder(getBaseContext());
                builder.setPositiveButton("Get Pro", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                final AlertDialog dialog = builder.create();
                LayoutInflater inflater = getLayoutInflater();
                View dialogLayout = inflater.inflate(R.layout.licence_alert, null);
                dialog.setView(dialogLayout);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                dialog.show();

                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface d) {
                        ImageView image = (ImageView) dialog.findViewById(R.id.goProDialogImage);
                        image.setImageResource(R.drawable.plus);
                    }
                });

            }
        });

        btn_add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Integer quantity = Integer.parseInt(txt_qty.getText().toString());

                try {

                    if (quantity < 0) {
                        Toast.makeText(getBaseContext(),
                                "Please enter a quantity of 0 or higher",
                                Toast.LENGTH_SHORT).show();
                        return;
                    }

                } catch (Exception e) {
                    Toast.makeText(getBaseContext(),
                            "Please enter a numeric quantity",
                            Toast.LENGTH_SHORT).show();

                    return;
                }


                ShoppingCartHelper.setQuantity(resultp, "" + (quantity * quantity) + "");
                ShoppingCartHelper.setProductId(resultp, serverdata_Arraylist.get(position).get("id"));
                ShoppingCartHelper.setSize(resultp, spn_size.getSelectedItem().toString());
                ShoppingCartHelper.setColor(resultp, spn_color.getSelectedItem().toString());
                finish();


            }
//            }
        });

        btn_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mPreferences.contains("AuthToken")) {
                    Intent i = new Intent(getBaseContext(), Chat.class);
                    i.putExtra("seller_name", resultp.get("seller_name"));
                    i.putExtra("seller_id", resultp.get("user_id"));
                    startActivity(i);
                }
            }
        });

        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareIt();
            }
        });
    }

    public class PagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    Tab_Item_Description tab_description = new Tab_Item_Description();
                    return tab_description;
                case 1:
                    if (category.equals("big_deal")) {
                        Tab_Item_Specification_clothing tab_specification = new Tab_Item_Specification_clothing();
                        return tab_specification;
                    }
                    if (category.equals("clothing")) {
                        Tab_Item_Specification_clothing tab_specification = new Tab_Item_Specification_clothing();
                        return tab_specification;
                    }
                    if (category.equals("cosmetic")) {
                        Tab_Item_Specification_clothing tab_specification = new Tab_Item_Specification_clothing();
                        return tab_specification;
                    }
                    if (category.equals("electronic")) {
                        Tab_Item_Specification_clothing tab_specification = new Tab_Item_Specification_clothing();
                        return tab_specification;
                    }
                    if (category.equals("bag")) {
                        Tab_Item_Bag tab_specification = new Tab_Item_Bag();
                        return tab_specification;
                    }
                    if (category.equals("phone")) {
                        Tab_Item_Specification_clothing tab_specification = new Tab_Item_Specification_clothing();
                        return tab_specification;
                    }
                    if (category.equals("shoe")) {
                        Tab_Item_Specification_clothing tab_specification = new Tab_Item_Specification_clothing();
                        return tab_specification;
                    }
                    if (category.equals("suggesstion")) {
                        Tab_Item_Specification_clothing tab_specification = new Tab_Item_Specification_clothing();
                        return tab_specification;
                    }

                    if (category.equals("home_appliance")) {
                        Tab_Item_Specification_clothing tab_specification = new Tab_Item_Specification_clothing();
                        return tab_specification;
                    }

                    if (category.equals("car_accessories")) {
                        Tab_Item_Car_Accessories tab_specification = new Tab_Item_Car_Accessories();
                        return tab_specification;
                    }

                    if (category.equals("sports")) {
                        Tab_Item_Specification_clothing tab_specification = new Tab_Item_Specification_clothing();
                        return tab_specification;
                    }

                    if (category.equals("medicine")) {
                        Tab_Item_Specification_clothing tab_specification = new Tab_Item_Specification_clothing();
                        return tab_specification;
                    }

                    if (category.equals("gift")) {
                        Tab_Item_Specification_clothing tab_specification = new Tab_Item_Specification_clothing();
                        return tab_specification;
                    }
                case 2:
                    Tab_Item_Rating tab_rating = new Tab_Item_Rating();
                    return tab_rating;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }

    private void shareIt() {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");

        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Share");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, "http://www.myanmardealer.com/my_conversation");
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }


    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            serverData = null;

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://www.myanmardealer.com/view_shop_licensce.txt?store_name=" +resultp.get("clothingStore_name"));
            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                serverData = EntityUtils.toString(httpEntity);
                Log.d("response", serverData);

                JSONObject jsonObject = new JSONObject(serverData);
                JSONArray jsonArray = jsonObject.getJSONArray("Rating");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject_licence = jsonArray.getJSONObject(i);
                    shop_licence_file_name = jsonObject_licence.getString("shop_licensce_file_name");
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

        }
    }
}
