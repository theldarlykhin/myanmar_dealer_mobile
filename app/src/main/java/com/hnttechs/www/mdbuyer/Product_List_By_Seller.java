package com.hnttechs.www.mdbuyer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 8/3/16.
 */
public class Product_List_By_Seller extends ActionBarActivity {
    static ListView lv_product_list_by_seller;
    Product_List_By_Seller_ListAdapter adapter;

    ProgressDialog mProgressDialog;
    static String serverData;
    ArrayList<HashMap<String, String>> arraylist;

    static SharedPreferences mPreferences;
    String User_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_list_by_seller);


        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        lv_product_list_by_seller = (ListView) findViewById(R.id.lv_product_list_by_seller);
        mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);
        User_id = mPreferences.getString("UserId", "0");

        new DataFetcherTask().execute();

    }


    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(Product_List_By_Seller.this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setMessage("loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            serverData = null;

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://www.myanmardealer.com/get_store_product_by_seller.txt?user_id=" + User_id);
            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                serverData = EntityUtils.toString(httpEntity);
                Log.d("response", serverData);

                JSONObject jsonObject = new JSONObject(serverData);
                JSONArray jsonArray = jsonObject.getJSONArray("Products");
                arraylist = new ArrayList<HashMap<String, String>>();

                for (int i = 0; i < jsonArray.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    JSONObject jsonObjectClothing = jsonArray.getJSONObject(i);

                    map.put("id", jsonObjectClothing.getString("id"));
                    map.put("store_name", jsonObjectClothing.getString("store_name"));
                    map.put("title", jsonObjectClothing.getString("title"));
                    map.put("category", jsonObjectClothing.getString("category"));
                    map.put("ingredient", jsonObjectClothing.getString("ingredient"));
                    map.put("usage", jsonObjectClothing.getString("usage"));
                    map.put("made_by_country", jsonObjectClothing.getString("made_by_country"));
                    map.put("description", jsonObjectClothing.getString("description"));
                    map.put("brand", jsonObjectClothing.getString("brand"));
                    map.put("effect", jsonObjectClothing.getString("effect"));
                    map.put("certification", jsonObjectClothing.getString("certification"));
                    map.put("age_group", jsonObjectClothing.getString("age_group"));
                    map.put("price", jsonObjectClothing.getString("price"));
                    map.put("feature", jsonObjectClothing.getString("feature"));
                    map.put("modal_number", jsonObjectClothing.getString("modal_number"));
                    map.put("color", jsonObjectClothing.getString("color"));
                    map.put("quantity", jsonObjectClothing.getString("quantity"));
                    map.put("size", jsonObjectClothing.getString("size"));
                    map.put("weight", jsonObjectClothing.getString("weight"));
                    map.put("made_with", jsonObjectClothing.getString("made_with"));
                    map.put("specification", jsonObjectClothing.getString("specification"));
                    map.put("thickness", jsonObjectClothing.getString("thickness"));
                    map.put("operation_system", jsonObjectClothing.getString("operation_system"));
                    map.put("bag_type", jsonObjectClothing.getString("bag_type"));
                    map.put("gender", jsonObjectClothing.getString("gender"));
                    map.put("footwear_type", jsonObjectClothing.getString("footwear_type"));
                    map.put("hat_type", jsonObjectClothing.getString("hat_type"));
                    map.put("power", jsonObjectClothing.getString("power"));
                    map.put("voltage", jsonObjectClothing.getString("voltage"));
                    map.put("dimension", jsonObjectClothing.getString("dimension"));
                    map.put("caution", jsonObjectClothing.getString("caution"));
                    map.put("charges", jsonObjectClothing.getString("charges"));
                    map.put("service_category", jsonObjectClothing.getString("service_category"));
                    map.put("fees", jsonObjectClothing.getString("fees"));
                    map.put("period", jsonObjectClothing.getString("period"));
                    map.put("school", jsonObjectClothing.getString("school"));
                    map.put("mmdealer_code", jsonObjectClothing.getString("mmdealer_code"));
                    map.put("avatar1_edit", jsonObjectClothing.getString("avatar1_edit"));
                    map.put("avatar2_edit", jsonObjectClothing.getString("avatar2_edit"));
                    map.put("avatar3_edit", jsonObjectClothing.getString("avatar3_edit"));
                    map.put("avatar4_edit", jsonObjectClothing.getString("avatar4_edit"));


                    arraylist.add(map);


                }

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mProgressDialog.dismiss();
            adapter = new Product_List_By_Seller_ListAdapter(getBaseContext(), arraylist);
            lv_product_list_by_seller.setAdapter(adapter);
            lv_product_list_by_seller.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
                    if (Product_List_By_Seller_ListAdapter.category.equals("bath_supplies")) {

                        Intent i = new Intent(getBaseContext(), Edit_Product_Bath_Supplies.class);
                        i.putExtra("id", arraylist.get(position).get("id"));
                        startActivity(i);
                    }
                    if (Product_List_By_Seller_ListAdapter.category.equals("beauty_equipments")) {

                    }
                    ;
                    if (Product_List_By_Seller_ListAdapter.category.equals("car_accessories")) {
                    }
                    if (Product_List_By_Seller_ListAdapter.category.equals("computers_laptops")) {

                    }
                    if (Product_List_By_Seller_ListAdapter.category.equals("eletronic_related")) {

                    }
                    if (Product_List_By_Seller_ListAdapter.category.equals("equipments")) {

                    }
                    if (Product_List_By_Seller_ListAdapter.category.equals("fashion_related")) {

                    }
                    if (Product_List_By_Seller_ListAdapter.category.equals("bags")) {

                    }
                    if (Product_List_By_Seller_ListAdapter.category.equals("cloths")) {
                        Intent i = new Intent(getBaseContext(), Edit_Product_Cloth.class);
                        i.putExtra("id", arraylist.get(position).get("id"));
                        startActivity(i);
                        finish();
                    }
                    if (Product_List_By_Seller_ListAdapter.category.equals("footwears")) {

                    }
                    if (Product_List_By_Seller_ListAdapter.category.equals("hats")) {

                    }
                    if (Product_List_By_Seller_ListAdapter.category.equals("gifts")) {

                    }
                    if (Product_List_By_Seller_ListAdapter.category.equals("home_appliance")) {

                    }
                    if (Product_List_By_Seller_ListAdapter.category.equals("instruments")) {

                    }
                    if (Product_List_By_Seller_ListAdapter.category.equals("machines")) {

                    }
                    if (Product_List_By_Seller_ListAdapter.category.equals("comesmetics")) {

                    }
                    if (Product_List_By_Seller_ListAdapter.category.equals("medicines")) {

                    }
                    if (Product_List_By_Seller_ListAdapter.category.equals("motorcycle")) {

                    }
                    if (Product_List_By_Seller_ListAdapter.category.equals("sports")) {

                    }
                    if (Product_List_By_Seller_ListAdapter.category.equals("phone_related")) {

                    }
                    if (Product_List_By_Seller_ListAdapter.category.equals("toys")) {

                    }


                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;
            case R.id.action_message:
                Intent message = new Intent(getBaseContext(),Message_Box.class);
                startActivity(message);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
