package com.hnttechs.www.mdbuyer;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 5/4/16.
 */
public class Product_List_By_Seller_ListAdapter extends BaseAdapter {


    // Declare Variables
    Context c;
    ArrayList<HashMap<String, String>> serverdata_arraylist;
    static String category;

    public Product_List_By_Seller_ListAdapter(Context context, ArrayList<HashMap<String, String>> arraylist) {
        c = context;
        serverdata_arraylist = arraylist;
    }

    @Override
    public int getCount() {
        return serverdata_arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {


        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.product_list_by_seller_listitem, parent, false);

            TextView title = (TextView) convertView.findViewById(R.id.title);
            TextView size = (TextView) convertView.findViewById(R.id.size);
            TextView color = (TextView) convertView.findViewById(R.id.color);
            TextView price = (TextView) convertView.findViewById(R.id.price);

            if (serverdata_arraylist.get(position).get("title") != null &&
                    !serverdata_arraylist.get(position).get("title").isEmpty() && !serverdata_arraylist.get(position).get("title").equals("null")) {
                title.setText(serverdata_arraylist.get(position).get("title"));
            }

            if (serverdata_arraylist.get(position).get("size") != null &&
                    !serverdata_arraylist.get(position).get("size").isEmpty() && !serverdata_arraylist.get(position).get("size").equals("null")) {
                size.setText(serverdata_arraylist.get(position).get("size"));
            }

            if (serverdata_arraylist.get(position).get("color") != null &&
                    !serverdata_arraylist.get(position).get("color").isEmpty() && !serverdata_arraylist.get(position).get("color").equals("null")) {
                color.setText(serverdata_arraylist.get(position).get("color"));
            }

            if (serverdata_arraylist.get(position).get("price") != null &&
                    !serverdata_arraylist.get(position).get("price").isEmpty() && !serverdata_arraylist.get(position).get("price").equals("null")) {
                price.setText(serverdata_arraylist.get(position).get("price"));
            }

            if (serverdata_arraylist.get(position).get("category") != null &&
                    !serverdata_arraylist.get(position).get("category").isEmpty() && !serverdata_arraylist.get(position).get("title").equals("null")) {
                category = serverdata_arraylist.get(position).get("category");

//                convertView.setOnClickListener(new View.OnClickListener() {
//
//                    @Override
//                    public void onClick(View arg0) {
//
//
//                    }
//                });
            }

        }

        return convertView;
    }
}
