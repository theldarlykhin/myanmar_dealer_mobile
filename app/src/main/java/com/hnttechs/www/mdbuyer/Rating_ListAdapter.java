package com.hnttechs.www.mdbuyer;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 7/31/16.
 */
public class Rating_ListAdapter extends BaseAdapter {

    // Declare Variables
    Context c;
    ArrayList<HashMap<String, String>> serverdata_arraylist;
    HashMap<String, String> resultp = new HashMap<String, String>();

    public Rating_ListAdapter(Context context,ArrayList<HashMap<String, String>> arraylist) {
        c = context;
        serverdata_arraylist = arraylist;
    }

    @Override
    public int getCount() {
        return serverdata_arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {


        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.tab_item_ratingitem, parent, false);
            TextView txt_rating_user_name = (TextView)convertView.findViewById(R.id.txt_rating_user_name);
            TextView txt_comment = (TextView)convertView.findViewById(R.id.txt_comment);
//            RatingBar rt_bar = (RatingBar)convertView.findViewById(R.id.rt_rating_bar);
//            rt_bar.setRating(2);

            resultp = serverdata_arraylist.get(position);

            txt_rating_user_name.setText(resultp.get("user_id"));
            txt_comment.setText(resultp.get("message"));

        }
        return convertView;
    }
}
