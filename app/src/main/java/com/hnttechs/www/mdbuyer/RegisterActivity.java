package com.hnttechs.www.mdbuyer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.savagelook.android.UrlJsonAsyncTask;

import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dell on 5/17/16.
 */
public class RegisterActivity extends ActionBarActivity {

    private final static String REGISTER_API_ENDPOINT_URL = "http://www.myanmardealer.com/myapi/v1/user_new";
    private SharedPreferences mPreferences;
    private String mUserEmail;
    private String mUserName;
    private String mUserType;
    private String mUserPassword;
    private String mUserPasswordConfirmation;


    private String mpersonal_name;
    private String mphone;
    private String mpersonal_address;
    private String mpersonal_nrc_number;


    private String mstaff_name;
    private String mstaff_nrc_number;
    private String mstaff_address;
    private String mcompany_licensce;
    private String mcompany_name;

    private String mretail_name;
    private String mretail_address;
    private String mretail_nrc_number;
    private String mshop_licensce;
    private String mshop_name;

    private ImageView img_staff_nrc_front;
    private ImageView img_staff_nrc_back;
    private ImageView img_personal_nrc_front;
    private ImageView img_personal_nrc_back;
    private ImageView img_retail_nrc_front;
    private ImageView img_retail_nrc_back;


    static String encoded1;
    static String encoded2;
    static String encoded3;
    static String encoded4;
    static String encoded5;
    static String encoded6;

    final List<String> list = new ArrayList<String>();
    static Spinner spn_user_level;
    int type = 0;


    private LinearLayout layout_personal;
    private LinearLayout layout_retail;
    private LinearLayout layout_company;

    private RadioGroup radioTypeGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_register);

        mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);


        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        list.add(0, "Seller");
        list.add(1, "Buyer");

        spn_user_level = (Spinner) findViewById(R.id.spn_user_level);
        ArrayAdapter<String> adp1 = new ArrayAdapter<String>(this, R.layout.spinner_item, list);

        adp1.setDropDownViewResource(R.layout.spinner_item);
        spn_user_level.setAdapter(adp1);

        Button staff_nrc_front = (Button) findViewById(R.id.btn_staff_nrc_front);
        Button staff_nrc_back = (Button) findViewById(R.id.btn_staff_nrc_back);
        Button personal_nrc_front = (Button) findViewById(R.id.btn_personal_nrc_front);
        Button personal_nrc_back = (Button) findViewById(R.id.btn_personal_nrc_back);
        Button retail_nrc_front = (Button) findViewById(R.id.btn_retail_nrc_front);
        Button retail_nrc_back = (Button) findViewById(R.id.btn_retail_nrc_back);

        layout_personal = (LinearLayout) findViewById(R.id.layout_personal);
        layout_retail = (LinearLayout) findViewById(R.id.layout_retail);
        layout_company = (LinearLayout) findViewById(R.id.layout_company);
        radioTypeGroup = (RadioGroup) findViewById(R.id.rdo_group_shop_type);

        spn_user_level.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(spn_user_level.getSelectedItem().toString().equals("Buyer")) {
                    layout_personal.setVisibility(View.GONE);
                    layout_retail.setVisibility(View.GONE);
                    layout_company.setVisibility(View.GONE);
                    radioTypeGroup.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

    });

        radioTypeGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rdo_personal) {

                    layout_personal.setVisibility(View.VISIBLE);
                    layout_company.setVisibility(View.GONE);
                    layout_retail.setVisibility(View.GONE);
                } else if (checkedId == R.id.rdo_company) {
                    layout_personal.setVisibility(View.GONE);
                    layout_company.setVisibility(View.VISIBLE);
                    layout_retail.setVisibility(View.GONE);
                } else if (checkedId == R.id.rdo_retail) {
                    layout_personal.setVisibility(View.GONE);
                    layout_company.setVisibility(View.GONE);
                    layout_retail.setVisibility(View.VISIBLE);
                }
            }
        });


        img_staff_nrc_front = (ImageView) findViewById(R.id.img_staff_nrc_front);
        img_staff_nrc_back = (ImageView) findViewById(R.id.img_staff_nrc_back);
        img_personal_nrc_front = (ImageView) findViewById(R.id.img_personal_nrc_front);
        img_personal_nrc_back = (ImageView) findViewById(R.id.img_personal_nrc_back);
        img_retail_nrc_front = (ImageView) findViewById(R.id.img_retail_nrc_front);
        img_retail_nrc_back = (ImageView) findViewById(R.id.img_retail_nrc_back);



        staff_nrc_front.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = 31;
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
            }
        });
        staff_nrc_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = 32;
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
            }
        });
        personal_nrc_front.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = 21;
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
            }
        });
        personal_nrc_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = 22;
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
            }
        });
        retail_nrc_front.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = 11;
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
            }
        });
        retail_nrc_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = 12;
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
            }
        });



    }

    public void registerNewAccount(View button) {
        EditText userEmailField = (EditText) findViewById(R.id.userEmail);


        if (spn_user_level.getSelectedItem().toString().equals("Seller")) {
            mUserType = "1";
        } else if (spn_user_level.getSelectedItem().toString().equals("Buyer")) {
            mUserType = "2";
        }



        EditText personal_name = (EditText) findViewById(R.id.txt_personal_name);
        EditText txtphone = (EditText) findViewById(R.id.txt_phone);
        EditText personal_address = (EditText) findViewById(R.id.txt_personal_address);
        EditText personal_nrc_number = (EditText) findViewById(R.id.txt_personal_nrd);


        EditText staff_name = (EditText) findViewById(R.id.txt_staff_name);
        EditText staff_nrc_number = (EditText) findViewById(R.id.txt_staff_nrc_number);
        EditText staff_address = (EditText) findViewById(R.id.txt_staff_address);
        EditText company_licensce = (EditText) findViewById(R.id.txt_company_licence);
        EditText company_name = (EditText) findViewById(R.id.txt_company_name);

        EditText retail_name = (EditText) findViewById(R.id.txt_retail_name);
        EditText retail_address = (EditText) findViewById(R.id.txt_retail_address);
        EditText retail_nrc_number = (EditText) findViewById(R.id.txt_retail_nrc);
        EditText shop_licensce = (EditText) findViewById(R.id.txt_shop_license);
        EditText shop_name = (EditText) findViewById(R.id.txt_shop_name);

        mpersonal_name = personal_name.getText().toString();
        mpersonal_address = personal_address.getText().toString();
        mpersonal_nrc_number = personal_nrc_number.getText().toString();
        mphone = txtphone.getText().toString();


        mstaff_name = staff_name.getText().toString();
        mstaff_nrc_number = staff_nrc_number.getText().toString();
        mstaff_address = staff_address.getText().toString();
        mcompany_licensce = company_licensce.getText().toString();
        mcompany_name = company_name.getText().toString();

        mretail_name = retail_name.getText().toString();
        mretail_address = retail_address.getText().toString();
        mretail_nrc_number = retail_nrc_number.getText().toString();
        mshop_licensce = shop_licensce.getText().toString();
        mshop_name = shop_name.getText().toString();


        mUserEmail = userEmailField.getText().toString();
        EditText userNameField = (EditText) findViewById(R.id.userName);
        mUserName = userNameField.getText().toString();
        EditText userPasswordField = (EditText) findViewById(R.id.userPassword);
        mUserPassword = userPasswordField.getText().toString();
        EditText userPasswordConfirmationField = (EditText) findViewById(R.id.userPasswordConfirmation);
        mUserPasswordConfirmation = userPasswordConfirmationField.getText().toString();
        if (mUserEmail.length() == 0 || mUserName.length() == 0 || mUserPassword.length() == 0 || mUserPasswordConfirmation.length() == 0) {
            // input fields are empty
            Toast.makeText(this, "Please complete all the fields",
                    Toast.LENGTH_LONG).show();
            return;
        } else {
            if (!mUserPassword.equals(mUserPasswordConfirmation)) {
                // password doesn't match confirmation
                Toast.makeText(this, "Your password doesn't match confirmation, check again",
                        Toast.LENGTH_LONG).show();
                return;
            } else {
                // everything is ok!
                RegisterTask registerTask = new RegisterTask(RegisterActivity.this);
                registerTask.setMessageLoading("Registering new account...");
                registerTask.execute(REGISTER_API_ENDPOINT_URL);
            }
        }
    }

    private class RegisterTask extends UrlJsonAsyncTask {
        public RegisterTask(Context context) {
            super(context);
        }

        @Override
        protected JSONObject doInBackground(String... urls) {
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(urls[0]);
            JSONObject holder = new JSONObject();
            JSONObject userObj = new JSONObject();
            String response = null;
            JSONObject json = new JSONObject();

            try {
                try {
                    // setup the returned values in case
                    // something goes wrong
                    json.put("success", false);

                    json.put("info", "Sign up SUCCESSFULLY.Please sign in to continue.");
                    // add the users's info to the post params
                    userObj.put("name", mUserName);
                    userObj.put("email", mUserEmail);
                    userObj.put("password", mUserPassword);
                    userObj.put("password_confirmation", mUserPasswordConfirmation);
                    userObj.put("user_type", mUserType);

                    userObj.put("personal_name", mpersonal_name);
                    userObj.put("phone", mphone);
                    userObj.put("personal_address", mpersonal_address);
                    userObj.put("personal_nrc_number", mpersonal_nrc_number);

                    userObj.put("staff_name", mstaff_name);
                    userObj.put("staff_nrc_number", mstaff_nrc_number);
                    userObj.put("staff_address", mstaff_address);
                    userObj.put("company_licensce", mcompany_licensce);
                    userObj.put("company_name", mcompany_name);

                    userObj.put("retail_name", mretail_name);
                    userObj.put("retail_address", mretail_address);
                    userObj.put("retail_nrc_number", mretail_nrc_number);
                    userObj.put("shop_licensce", mshop_licensce);
                    userObj.put("shop_name", mshop_name);

                    userObj.put("personal_nrc_front", encoded3);
                    userObj.put("personal_nrc_back", encoded4);

                    userObj.put("staff_nrc_front", encoded5);
                    userObj.put("staff_nrc_back", encoded6);
                    userObj.put("retail_nrc_front", encoded1);
                    userObj.put("retail_nrc_back", encoded2);

//                    holder.put("user", userObj);
//                    holder.put(userObj);
                    StringEntity se = new StringEntity(userObj.toString());
                    post.setEntity(se);

                    // setup the request headers
                    post.setHeader("Accept", "application/json");
                    post.setHeader("Content-Type", "application/json");

                    ResponseHandler<String> responseHandler = new BasicResponseHandler();
                    response = client.execute(post, responseHandler);
                    json = new JSONObject(response);

                } catch (HttpResponseException e) {
                    e.printStackTrace();
                    Log.e("ClientProtocol", "" + e);
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("IO", "" + e);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("JSON", "" + e);
            }

            return json;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            finish();
            try {
                if (json.getBoolean("success")) {
                    // everything is ok
                    SharedPreferences.Editor editor = mPreferences.edit();
                    // save the returned auth_token into
                    // the SharedPreferences
                    editor.putString("AuthToken", json.getJSONObject("data").getString("auth_token"));
                    editor.putString("UserId", json.getJSONObject("data").getString("user_id"));
                    editor.putString("UserName", json.getJSONObject("data").getString("user_name"));
                    editor.putString("Email", json.getJSONObject("data").getString("email"));
                    editor.putString("user_type_id", json.getJSONObject("data").getString("user_type_id"));
                    editor.commit();

                    // launch the HomeActivity and close this one
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                Toast.makeText(context, json.getString("info"), Toast.LENGTH_LONG).show();
//                Toast.makeText(context, json.toString(), Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                // something went wrong: show a Toast
                // with the exception message
                Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
            } finally {
                super.onPostExecute(json);
            }
        }
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
//            case 0:
//                if(resultCode == RESULT_OK){
//                    Uri selectedImage = imageReturnedIntent.getData();
//                    if(upload_image_no==0) {
//                        preview_image1.setImageURI(selectedImage);
//                    }
//                    if(upload_image_no==1) {
//                        preview_image2.setImageURI(selectedImage);
//                    }
//                    if(upload_image_no==2) {
//                        preview_image3.setImageURI(selectedImage);
//                    }
//                    if(upload_image_no==3) {
//                        preview_image4.setImageURI(selectedImage);
//                    }
//                    upload_image_no++;
//                }
//
//                break;
            case 1:
                try {
                    if (resultCode == RESULT_OK) {
                        Uri selectedImage = imageReturnedIntent.getData();
                        if (type == 11) {
                            img_retail_nrc_front.setImageURI(selectedImage);
                            Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));


                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                            byte[] byteArray = byteArrayOutputStream.toByteArray();

                            encoded1 = "data:image/png;base64," + Base64.encodeToString(byteArray, Base64.DEFAULT);

                        }
                        if (type == 12) {
                            img_retail_nrc_back.setImageURI(selectedImage);

                            Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));


                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                            byte[] byteArray = byteArrayOutputStream.toByteArray();

                            encoded2 = "data:image/png;base64," + Base64.encodeToString(byteArray, Base64.DEFAULT);

                        }
                        if (type == 21) {
                            img_personal_nrc_front.setImageURI(selectedImage);

                            Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));


                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                            byte[] byteArray = byteArrayOutputStream.toByteArray();

                            encoded3 = "data:image/png;base64," + Base64.encodeToString(byteArray, Base64.DEFAULT);

                        }
                        if (type == 22) {
                            img_personal_nrc_back.setImageURI(selectedImage);

                            Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));


                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                            byte[] byteArray = byteArrayOutputStream.toByteArray();

                            encoded4 = "data:image/png;base64," + Base64.encodeToString(byteArray, Base64.DEFAULT);

                        }

                        if (type == 31) {
                            img_staff_nrc_front.setImageURI(selectedImage);

                            Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));


                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                            byte[] byteArray = byteArrayOutputStream.toByteArray();

                            encoded5 = "data:image/png;base64," + Base64.encodeToString(byteArray, Base64.DEFAULT);

                        }
                        if (type == 32) {
                            img_staff_nrc_back.setImageURI(selectedImage);

                            Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));


                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                            byte[] byteArray = byteArrayOutputStream.toByteArray();

                            encoded6 = "data:image/png;base64," + Base64.encodeToString(byteArray, Base64.DEFAULT);

                        }
                    }
                } catch (IOException e) {
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;
            case R.id.action_message:
                Intent message = new Intent(getBaseContext(),Message_Box.class);
                startActivity(message);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
