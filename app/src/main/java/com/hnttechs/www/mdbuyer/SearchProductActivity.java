package com.hnttechs.www.mdbuyer;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;

import com.savagelook.android.Lazy;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 3/2/16.
 */
public class SearchProductActivity extends ActionBarActivity {

    GridView gridView;
    BigDeal_GridViewAdapter adapter;
    static String serverData;
    ArrayList<HashMap<String, String>> arraylist;
    static String search_value;


    private MenuItem mSearchAction;
    private boolean isSearchOpened = false;
    private EditText edtSeach;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_product);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent i = getIntent();
        search_value = i.getStringExtra("search_value");

        gridView = (GridView) findViewById(R.id.gdv_search_result);
        NetworkUtils utils = new NetworkUtils(getBaseContext());
        if (utils.isConnectingToInternet()) {
            new DataFetcherTask().execute();
        } else {
        }
    }


    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            serverData = null;

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://www.myanmardealer.com/seach_from_mobile.txt?search=" + search_value);
            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                serverData = EntityUtils.toString(httpEntity);
                Log.d("response", serverData);

                JSONObject jsonObject = new JSONObject(serverData);
                JSONArray jsonArray = jsonObject.getJSONArray("Products");
                arraylist = new ArrayList<HashMap<String, String>>();

                for (int i = 0; i < jsonArray.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    JSONObject jsonObjectClothing = jsonArray.getJSONObject(i);


                    map.put("clothingName", jsonObjectClothing.getString("title"));
                    map.put("clothingPrice", jsonObjectClothing.getString("price"));
                    map.put("clothingQuantity", jsonObjectClothing.getString("quantity"));
                    map.put("clothingStore_name", jsonObjectClothing.getString("store_name"));
                    map.put("size", jsonObjectClothing.getString("size"));
                    map.put("color", jsonObjectClothing.getString("color"));
                    map.put("description", jsonObjectClothing.getString("description"));
                    map.put("made_by_country", jsonObjectClothing.getString("made_by_country"));
                    map.put("brand", jsonObjectClothing.getString("brand"));
                    map.put("avatar1_edit", jsonObjectClothing.getString("avatar1_edit"));
                    map.put("avatar2_edit", jsonObjectClothing.getString("avatar2_edit"));
                    map.put("avatar3_edit", jsonObjectClothing.getString("avatar3_edit"));
                    map.put("avatar4_edit", jsonObjectClothing.getString("avatar4_edit"));
                    map.put("user_id", jsonObjectClothing.getString("user_id"));
                    map.put("id", jsonObjectClothing.getString("id"));
                    map.put("seller_name", jsonObjectClothing.getString("seller_name"));

                    arraylist.add(map);


                }

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            adapter = new BigDeal_GridViewAdapter(getBaseContext(), arraylist, false);
            gridView.setAdapter(adapter);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(getBaseContext(), ProductDetailActivity.class);
                    i.putExtra("Position", position);
                    i.putExtra("arraylist", arraylist);
                    i.putExtra("category", "clothing");
                    startActivity(i);
                }
            });
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;
            case R.id.action_message:
                Intent message = new Intent(getBaseContext(),Message_Box.class);
                startActivity(message);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }
}