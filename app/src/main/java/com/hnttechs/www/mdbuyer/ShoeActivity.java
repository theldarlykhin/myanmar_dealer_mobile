package com.hnttechs.www.mdbuyer;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 3/2/16.
 */
public class ShoeActivity extends ActionBarActivity {

    GridView gridView;
    BigDeal_GridViewAdapter adapter;
    static String serverData;
    static ArrayList<HashMap<String, String>> arraylist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shoe);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        gridView = (GridView) findViewById(R.id.gdv_clothing);
        NetworkUtils utils = new NetworkUtils(getBaseContext());
        if (utils.isConnectingToInternet()) {
            new DataFetcherTask().execute();
        } else {
        }

    }

    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            serverData = null;

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://www.myanmardealer.com/my_api_fashion_footwears.txt");
            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                serverData = EntityUtils.toString(httpEntity);
                Log.d("response", serverData);

                JSONObject jsonObject = new JSONObject(serverData);
                JSONArray jsonArray = jsonObject.getJSONArray("Products");
                arraylist = new ArrayList<HashMap<String, String>>();

                for (int i = 0; i < jsonArray.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    JSONObject jsonObjectClothing = jsonArray.getJSONObject(i);

                    map.put("clothingName", jsonObjectClothing.getString("title"));
                    map.put("clothingPrice", jsonObjectClothing.getString("price"));
                    map.put("clothingQuantity", "5");
                    map.put("clothingStore_name", jsonObjectClothing.getString("store_name"));
                    map.put("size", jsonObjectClothing.getString("size"));
                    map.put("color", jsonObjectClothing.getString("color"));
                    map.put("description", jsonObjectClothing.getString("description"));
                    map.put("made_by_country", jsonObjectClothing.getString("made_by_country"));
                    map.put("brand", jsonObjectClothing.getString("brand"));
                    map.put("cloth_photo", jsonObjectClothing.getString("avatar1_file_name"));
                    map.put("user_id", jsonObjectClothing.getString("user_id"));
                    map.put("seller_name", jsonObjectClothing.getString("seller_name"));

                    arraylist.add(map);


                }

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            adapter = new BigDeal_GridViewAdapter(getBaseContext(), arraylist, false);
            gridView.setAdapter(adapter);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(getBaseContext(), ProductDetailActivity.class);
                    i.putExtra("come_from", "shoe");
                    i.putExtra("Position", position);
                    i.putExtra("category", "shoe");
                    startActivity(i);
                }
            });
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                startActivity(homeIntent);
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}