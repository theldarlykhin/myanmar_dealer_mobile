package com.hnttechs.www.mdbuyer;

import com.hnttechs.www.mdbuyer.model.Clothing;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 12/24/15.
 */
public class ShoppingCartEntry {
    private HashMap<String, String> mClothing;
    private String mQuantity;
    private String mSize;
    private String mProductId;
    private String mColor;
    private String mShopName;


    public ShoppingCartEntry(HashMap<String, String> clothing, String quantity) {
        mClothing = clothing;
        mQuantity = quantity;
    }

    public HashMap<String, String> getProduct() {
        return mClothing;
    }

    public String getQuantity() {
        return mQuantity;
    }

    public void setQuantity(String quantity) {
        mQuantity = quantity;
    }

    public String getProductId() {
        return mProductId;
    }

    public void setProductId(String ProductId) {
        mProductId = ProductId;
    }



    public String getSize() {
        return mSize;
    }

    public void setSize(String size) {
        mSize = size;
    }

    public String getColor() {
        return mColor;
    }

    public void setColor(String color) {
        mColor = color;
    }

}
