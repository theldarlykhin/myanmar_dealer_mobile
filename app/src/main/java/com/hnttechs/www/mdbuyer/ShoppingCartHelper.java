package com.hnttechs.www.mdbuyer;

import android.content.res.Resources;

import com.hnttechs.www.mdbuyer.model.Clothing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

/**
 * Created by dell on 12/23/15.
 */
public class ShoppingCartHelper {
    public static final String PRODUCT_INDEX = "PRODUCT_INDEX";
    private static Map<HashMap<String, String>, ShoppingCartEntry> cartMap = new HashMap<HashMap<String, String>, ShoppingCartEntry>();

    public static void setQuantity(HashMap<String, String> clothing, String quantity) {
        ShoppingCartEntry curEntry = cartMap.get(clothing);
//        if(quantity <= 0) {
//            if(curEntry != null)
//                removeProduct(clothing);
//            return;
//        }

        if(curEntry == null) {
            curEntry = new ShoppingCartEntry(clothing, quantity);
            cartMap.put(clothing, curEntry);
            return;
        }
        curEntry.setQuantity(quantity);
    }

    public static String getProductQuantity(HashMap<String, String> product) {
        ShoppingCartEntry curEntry = cartMap.get(product);

        if(curEntry != null)
            return curEntry.getQuantity();

        return "null";
    }


    public static void setProductId(HashMap<String, String> clothing, String productId) {
        ShoppingCartEntry curEntry = cartMap.get(clothing);

        curEntry.setProductId(productId);
    }

    public static String getProductId(HashMap<String, String> product) {
        ShoppingCartEntry curEntry = cartMap.get(product);

        if(curEntry != null)
            return curEntry.getProductId();

        return "null";
    }

    public static void setSize(HashMap<String, String> clothing, String Size) {
        ShoppingCartEntry curEntry = cartMap.get(clothing);

        curEntry.setSize(Size);
    }

    public static String getSize(HashMap<String, String> product) {
        ShoppingCartEntry curEntry = cartMap.get(product);

        if(curEntry != null)
            return curEntry.getSize();

        return "null";
    }

    public static void setColor(HashMap<String, String> clothing, String color) {
        ShoppingCartEntry curEntry = cartMap.get(clothing);

        curEntry.setColor(color);
    }

    public static String getColor(HashMap<String, String> product) {
        ShoppingCartEntry curEntry = cartMap.get(product);

        if(curEntry != null)
            return curEntry.getColor();

        return "null";
    }


    public static int getitemcount() {
        Integer count = cartMap.keySet().size();

        return count;
    }

    public static void removeProduct(HashMap<String, String> product) {
        cartMap.remove(product);
    }

    public static ArrayList<HashMap<String, String>> getCartList() {
        ArrayList<HashMap<String, String>> cartList = new ArrayList<HashMap<String, String>>(cartMap.keySet().size());
        for(HashMap<String, String> p : cartMap.keySet()) {
            cartList.add(p);
        }
        return cartList;
    }
}
