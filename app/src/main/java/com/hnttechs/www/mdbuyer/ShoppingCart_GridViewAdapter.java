package com.hnttechs.www.mdbuyer;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hnttechs.www.mdbuyer.model.Clothing;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 7/15/15.
 */
public class ShoppingCart_GridViewAdapter extends BaseAdapter {

    private Context context;
    ArrayList<HashMap<String, String>> listData;
    private boolean mShowQuantity;
    static EditText txt_qty;
    static CheckBox chk_order;
    static HashMap<String, String> clothing;
    static String product_id;
    static int var_qty;
    static String image_string1;
    static String product_name;

    public ShoppingCart_GridViewAdapter(Context context, ArrayList<HashMap<String, String>> listData, Boolean showQuantity) {
        this.context        = context;
        this.listData       = listData;
        mShowQuantity       = showQuantity;
    }

    @Override
    public int getCount() { return listData.size(); }

    @Override
    public Object getItem(int position) { return null; }

    @Override
    public long getItemId(int position) { return 0; }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if (convertView == null) {
            gridView = new View(context);
            gridView = inflater.inflate(R.layout.shoppingcart_list_item, null);

            ImageView img_cloth_photo = (ImageView) gridView.findViewById(R.id.img_clothing);
            final TextView lbl_price = (TextView) gridView
                    .findViewById(R.id.txt_price);
            TextView lbl_name = (TextView) gridView
                    .findViewById(R.id.lbl_name);
            TextView lbl_size = (TextView) gridView
                    .findViewById(R.id.txt_size);
            TextView lbl_color = (TextView) gridView
                    .findViewById(R.id.txt_color);
            TextView lbl_shop_name = (TextView) gridView
                    .findViewById(R.id.lbl_shop_name);
            TextView qty = (TextView) gridView
                    .findViewById(R.id.qty);

            chk_order = (CheckBox)gridView.findViewById(R.id.chk_item);


        clothing = listData.get(position);
        final Integer clothingPrice = Integer.parseInt(clothing.get("clothingPrice").toString());
            lbl_name.setText(clothing.get("clothingName"));

            var_qty =Integer.parseInt(ShoppingCartHelper.getProductQuantity(clothing));
            lbl_size.setText(ShoppingCartHelper.getSize(clothing));
            lbl_color.setText(ShoppingCartHelper.getColor(clothing));
            lbl_price.setText("MMK " + Integer.parseInt(clothing.get("clothingPrice")) * var_qty + " Kyats");
            lbl_shop_name.setText(clothing.get("clothingStore_name"));

//            if (clothing.get("avatar1_edit") != null && !clothing.get("avatar1_edit").isEmpty() &&
//                    !clothing.get("avatar1_edit").equals("null")) {
//                image_string1 = clothing.get("avatar1_edit").toString();
//            }
//
//            if (image_string1 != null && !image_string1.isEmpty() && !image_string1.equals("null")) {
//                String base64_string1 = image_string1.substring(22, image_string1.length());
//                byte[] decodedString1 = Base64.decode(base64_string1, Base64.DEFAULT);
//                img_cloth_photo.setImageBitmap(BitmapFactory.decodeByteArray(decodedString1, 0, decodedString1.length));
//            }

            product_name = clothing.get("title");
            qty.setText("" + var_qty + "");

            product_id = ShoppingCartHelper.getProductId(clothing);


            chk_order.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    Integer old_amount = Integer.parseInt(Tab_ShoppingCartFragment.total_price.getText().toString());
                    Integer total = old_amount + clothingPrice;
                    Tab_ShoppingCartFragment.total_price.setText(total.toString());
                }
            });

        } else { gridView = (View) convertView; }
        return gridView;
    }
}
