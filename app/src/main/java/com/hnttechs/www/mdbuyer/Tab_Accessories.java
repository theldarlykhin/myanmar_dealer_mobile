package com.hnttechs.www.mdbuyer;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

/**
 * Created by dell on 3/2/16.
 */
public class Tab_Accessories extends Fragment {
    GridView gridView;
    static final String[ ] GRID_DATA = new String[] {
            "25,000 Kyats" ,
            "10,000 Kyats",
            "30,000 Kyats" ,
            "23,000 Kyats",
            "3,000 Kyats" ,
            "2,000 Kyats"
    };
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.clothing,container,false);

        gridView = (GridView) rootview.findViewById(R.id.gdv_clothing);
        gridView.setAdapter(new Accessories_GridViewAdapter(rootview.getContext(), GRID_DATA));
        gridView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {

                    public void onItemClick(AdapterView<?> parent, View v,
                                            int position, long id) {
                        Intent i = new Intent(getActivity(), ProductDetailActivity.class);
                        i.putExtra("ProductType","Accessories");
                        i.putExtra("ProductName", GRID_DATA[position]);
                        startActivity(i);
                    }
                });

        return rootview;
    }
}
