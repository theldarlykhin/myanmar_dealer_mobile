package com.hnttechs.www.mdbuyer;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 7/12/16.
 */
public class Tab_BigDeal extends Fragment {

    BigDeal_GridViewAdapter adapter;
    static ArrayList<HashMap<String, String>> arraylist;
    static String serverData;
    static GridView gdv_big_deal;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.tab_bigdeal, container, false);
        gdv_big_deal = (GridView) rootview.findViewById(R.id.gdv_big_deal);

        NetworkUtils utils = new NetworkUtils(getContext());
        if (utils.isConnectingToInternet()) {
            new DataFetcherTask().execute();
        } else {
        }

        return  rootview;
    }


    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            serverData = null;

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://www.myanmardealer.com/my_big_deals.txt");
            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                serverData = EntityUtils.toString(httpEntity);
                Log.d("response", serverData);

                JSONObject jsonObject = new JSONObject(serverData);
                JSONArray jsonArray = jsonObject.getJSONArray("Products");
                arraylist = new ArrayList<HashMap<String, String>>();

                for (int i = 0; i < jsonArray.length(); i++) {
                    HashMap<String, String> map_big_deals = new HashMap<String, String>();
                    JSONObject jsonObject_big_deals = jsonArray.getJSONObject(i);

                    map_big_deals.put("clothingName", jsonObject_big_deals.getString("title"));
                    map_big_deals.put("clothingPrice", jsonObject_big_deals.getString("price"));
                    map_big_deals.put("clothingQuantity", "5");
                    map_big_deals.put("clothingStore_name", jsonObject_big_deals.getString("store_name"));
                    map_big_deals.put("size", jsonObject_big_deals.getString("size"));
                    map_big_deals.put("color", jsonObject_big_deals.getString("color"));
                    map_big_deals.put("description", jsonObject_big_deals.getString("description"));
                    map_big_deals.put("made_by_country", jsonObject_big_deals.getString("made_by_country"));
                    map_big_deals.put("brand", jsonObject_big_deals.getString("brand"));
                    map_big_deals.put("avatar1_edit", jsonObject_big_deals.getString("avatar1_edit"));
                    map_big_deals.put("avatar2_edit", jsonObject_big_deals.getString("avatar2_edit"));
                    map_big_deals.put("avatar3_edit", jsonObject_big_deals.getString("avatar3_edit"));
                    map_big_deals.put("avatar4_edit", jsonObject_big_deals.getString("avatar4_edit"));
                    map_big_deals.put("user_id", jsonObject_big_deals.getString("user_id"));
                    map_big_deals.put("id", jsonObject_big_deals.getString("id"));
                    map_big_deals.put("seller_name", jsonObject_big_deals.getString("seller_name"));

                    arraylist.add(map_big_deals);

                }



            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (arraylist != null) {
                adapter = new BigDeal_GridViewAdapter(getContext(), arraylist, false);
                gdv_big_deal.setAdapter(adapter);
                gdv_big_deal.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent i = new Intent(getContext(), ProductDetailActivity.class);
                        i.putExtra("Position", position);
                        i.putExtra("come_from", "big_deal");
                        i.putExtra("category", "big_deal");
                        startActivity(i);
                    }
                });
            }
        }
    }

}
