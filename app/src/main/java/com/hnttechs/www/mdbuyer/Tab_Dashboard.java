package com.hnttechs.www.mdbuyer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by dell on 7/17/16.
 */
public class Tab_Dashboard extends Fragment {
    static SharedPreferences mPreferences;
    String User_type_id;
    String Has_store;
    static Button btn_log_out;
    static TextView apply_as_seller;
    static LinearLayout layout_buyer;
    static LinearLayout layout_seller;
    static LinearLayout layout_create_store;
    static LinearLayout layout_create_product;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.tab_dashboard, container, false);

        ImageView btn_create_product = (ImageView)rootview.findViewById(R.id.btn_create_product);
        ImageView img_order_item = (ImageView)rootview.findViewById(R.id.img_order_item);
        ImageView img_order_confirm = (ImageView)rootview.findViewById(R.id.img_order_confirm);
        ImageView img_order_deliver = (ImageView)rootview.findViewById(R.id.img_order_deliver);
        ImageView img_order_received = (ImageView)rootview.findViewById(R.id.img_order_received);
        ImageView img_product_manage = (ImageView)rootview.findViewById(R.id.img_product_manage);
        ImageView img_order_management = (ImageView)rootview.findViewById(R.id.img_order_management);
        ImageView img_shop_manage = (ImageView)rootview.findViewById(R.id.img_shop_manage);
        btn_log_out = (Button)rootview.findViewById(R.id.btn_logout);
        final Button btn_register = (Button)rootview.findViewById(R.id.btn_register);

        apply_as_seller = (TextView)rootview.findViewById(R.id.layout_sellerDashboard);
        layout_seller = (LinearLayout)rootview.findViewById(R.id.layout_seller);
        layout_buyer = (LinearLayout)rootview.findViewById(R.id.layout_buyer);
        layout_create_product = (LinearLayout)rootview.findViewById(R.id.layout_create_product);
        layout_create_store = (LinearLayout)rootview.findViewById(R.id.layout_create_store);

        mPreferences = getActivity().getSharedPreferences("CurrentUser", Context.MODE_PRIVATE);

        User_type_id = mPreferences.getString("user_type_id", "2");
        Has_store = mPreferences.getString("Has_Store", "0");

        if (mPreferences.contains("AuthToken")) {
            btn_log_out.setText("Log Out");
            if (User_type_id.equals("1")) {

                btn_register.setVisibility(View.GONE);
                layout_seller.setVisibility(View.VISIBLE);
                layout_buyer.setVisibility(View.VISIBLE);
                apply_as_seller.setText("Seller Dashboard");
                if(Has_store.equals("yes")) {
                    layout_create_store.setVisibility(View.GONE);
                    layout_create_product.setVisibility(View.VISIBLE);
                } else {
                    layout_create_store.setVisibility(View.VISIBLE);
                    layout_create_product.setVisibility(View.GONE);
                }
            } else if (User_type_id.equals("2")) {
                layout_seller.setVisibility(View.GONE);
                apply_as_seller.setVisibility(View.GONE);
                btn_register.setVisibility(View.GONE);
            }
        } else {
            btn_log_out.setText("Log In");
            apply_as_seller.setVisibility(View.GONE);
            layout_buyer.setVisibility(View.GONE);
            btn_register.setVisibility(View.VISIBLE);
        }


        final ImageView btn_create_store = (ImageView)rootview.findViewById(R.id.btn_create_store);
        btn_create_store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), New_Store.class);
                startActivity(i);
            }
        });

        btn_log_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPreferences.contains("AuthToken")) {
                    mPreferences = getActivity().getSharedPreferences("CurrentUser", 0);
                    SharedPreferences.Editor editor = mPreferences.edit();
                    editor.clear();
                    editor.commit();
                    btn_log_out.setText("Log In");
                    apply_as_seller.setVisibility(View.GONE);
                    layout_buyer.setVisibility(View.GONE);
                    layout_seller.setVisibility(View.GONE);
                    btn_register.setVisibility(View.VISIBLE);
                } else {
                    Intent login = new Intent(getActivity(), LogInFragment.class);
                    startActivity(login);
                }
            }
        });

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), RegisterActivity.class);
                startActivity(i);
            }
        });

        btn_create_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent create_product = new Intent(getContext(),Create_Product.class);
                startActivity(create_product);
            }
        });

        img_order_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent order_list = new Intent(getContext(),Order_List.class);
                startActivity(order_list);

            }
        });

        img_order_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent order_confirmlist = new Intent(getContext(),Order_Confirm_List.class);
                startActivity(order_confirmlist);

            }
        });

        img_order_deliver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent order_confirmlist = new Intent(getContext(),Order_deliverList.class);
                startActivity(order_confirmlist);
            }
        });

        img_order_received.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent order_confirmlist = new Intent(getContext(),Order_receiveList.class);
                startActivity(order_confirmlist);

            }
        });

        img_product_manage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent_product_list_by_seller = new Intent(getContext(),Product_List_By_Seller.class);
                startActivity(intent_product_list_by_seller);
            }
        });

        img_order_management.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent_order_list_by_seller = new Intent(getContext(),Seller_Order_List.class);
                startActivity(intent_order_list_by_seller);
            }
        });

        img_shop_manage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent edit_store = new Intent(getContext(),Edit_Store.class);
                startActivity(edit_store);
            }
        });

        return  rootview;
    }

}
