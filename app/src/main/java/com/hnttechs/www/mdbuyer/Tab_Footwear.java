package com.hnttechs.www.mdbuyer;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.hnttechs.www.mdbuyer.model.Clothing;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by dell on 3/2/16.
 */
public class Tab_Footwear extends Fragment {

    GridView gridView;
    Clothing_GridViewAdapter adapter;
    static ArrayList<Clothing> clothinglist;
    ClothingDbHandler handler;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.clothing,container,false);

        gridView = (GridView) rootview.findViewById(R.id.gdv_clothing);
        handler = new ClothingDbHandler(rootview.getContext());
        NetworkUtils utils = new NetworkUtils(rootview.getContext());
//        if (handler.getClothingCount() == 0 && utils.isConnectingToInternet()) {
        if (utils.isConnectingToInternet()) {
            new DataFetcherTask().execute();
        } else {
            clothinglist = handler.getAllClothing();
//            adapter = new Clothing_GridViewAdapter(getContext(), clothinglist, false);
//            gridView.setAdapter(adapter);
//            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    Intent i = new Intent(getActivity(), ProductDetailActivity.class);
//                    i.putExtra("ProductType", "Clothing");
//                    i.putExtra("Position",position);
//                    startActivity(i);
//                }
//            });
        }
        return rootview;
    }

    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverData = null;

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://www.myanmardealer.com/my_api_fashion_footwears.txt");
            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                serverData = EntityUtils.toString(httpEntity);
                Log.d("response", serverData);
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                JSONObject jsonObject = new JSONObject(serverData);
                JSONArray jsonArray = jsonObject.getJSONArray("Products");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObjectClothing = jsonArray.getJSONObject(i);
                    String clothingName = jsonObjectClothing.getString("title");
                    int clothingPrice = jsonObjectClothing.getInt("price");
                    int clothingQuantity = jsonObjectClothing.getInt("quantity");
                    String clothingSize = jsonObjectClothing.getString("size");
                    String clothingMade_by_country = jsonObjectClothing.getString("made_by_country");
                    String clothingMade_with = jsonObjectClothing.getString("made_with");
                    String clothingDescription = jsonObjectClothing.getString("description");
                    String clothingGender = jsonObjectClothing.getString("gender");
                    String clothingBrand = jsonObjectClothing.getString("brand");
                    int clothingSeller_id = jsonObjectClothing.getInt("seller_id");
                    String clothingColor = jsonObjectClothing.getString("color");
                    String clothingStore_name = jsonObjectClothing.getString("store_name");
                    String clothingAvater1 = jsonObjectClothing.getString("avatar1_file_name");
                    String clothingAvater2 = jsonObjectClothing.getString("avatar2_file_name");
                    String clothingAvater3 = jsonObjectClothing.getString("avatar3_file_name");
                    String clothingAvater4 = jsonObjectClothing.getString("avatar4_file_name");
                    String clothingAvater5 = jsonObjectClothing.getString("avatar5_file_name");

                    Clothing clothing = new Clothing();
                    clothing.setTitle(clothingName);
                    clothing.setPrice(clothingPrice);
                    clothing.setQuantity(clothingQuantity);
                    clothing.setSize(clothingSize);
                    clothing.setMade_by_country(clothingMade_by_country);
                    clothing.setMade_with(clothingMade_with);
                    clothing.setDescription(clothingDescription);
                    clothing.setGender(clothingGender);
                    clothing.setBrand(clothingBrand);
                    clothing.setSeller_id(clothingSeller_id);
                    clothing.setColor(clothingColor);
                    clothing.setStore_name(clothingStore_name);
                    clothing.setAvater1(clothingAvater1);
                    clothing.setAvater2(clothingAvater2);
                    clothing.setAvater3(clothingAvater3);
                    clothing.setAvater4(clothingAvater4);
                    clothing.setAvater5(clothingAvater5);

                    handler.addClothing(clothing);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            clothinglist = handler.getAllClothing();
//            adapter = new Clothing_GridViewAdapter(getActivity().getBaseContext(), clothinglist, false);
//            gridView.setAdapter(adapter);
//            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    Intent i = new Intent(getActivity(), ProductDetailActivity.class);
//                    i.putExtra("ProductType", "Clothing");
//                    i.putExtra("Position", position);
//                    startActivity(i);
//                }
//            });
        }
    }
}