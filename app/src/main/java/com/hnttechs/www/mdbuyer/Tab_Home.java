package com.hnttechs.www.mdbuyer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Handler;

/**
 * Created by dell on 7/20/16.
 */
public class Tab_Home extends Fragment {

    GridView gdv_big_deal, gdv_woman_fashion, gdv_beauty_health,
            gdv_computer, gdv_electronic, gdv_fashion_accessories, gdv_kid, gdv_luggage_bags,
            gdv_mobile, gdv_shoe, gdv_suggesstion;
    BigDeal_GridViewAdapter bigdeal_adapter;
    BigDeal_GridViewAdapter adapter;
    BigDeal_GridViewAdapter adapter_accessories;
    BigDeal_GridViewAdapter adapter_bag;
    BigDeal_GridViewAdapter adapter_shoe;
    BigDeal_GridViewAdapter adapter_beauty;
    BigDeal_GridViewAdapter adapter_toy;
    BigDeal_GridViewAdapter adapter_phone;
    BigDeal_GridViewAdapter adapter_computer;
    BigDeal_GridViewAdapter adapter_electronic;

    ProgressDialog mProgressDialog;
    static String serverData;
    static ArrayList<HashMap<String, String>> arraylist, arraylist_big_deals;
    static ArrayList<HashMap<String, String>> arraylist_accessories;
    static ArrayList<HashMap<String, String>> arraylist_bag;
    static ArrayList<HashMap<String, String>> arraylist_shoe;
    static ArrayList<HashMap<String, String>> arraylist_beauty;
    static ArrayList<HashMap<String, String>> arraylist_toy;
    static ArrayList<HashMap<String, String>> arraylist_phone;
    static ArrayList<HashMap<String, String>> arraylist_computer;
    static ArrayList<HashMap<String, String>> arraylist_electronic;
    static ArrayList<Bitmap> bitmap_hot_item = new ArrayList<Bitmap>();
    ViewPager viewpager;
    static String image_string;
    public int currentimageindex = 0;
    ImageView slidingimage;

    static RelativeLayout title_big_deals, title_fashion, title_bag, title_shoe, title_beauty, title_phone,
            title_electronic, title_md_suggestion;

    static int condition_refresh;
    private SwipeRefreshLayout swipeContainer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.tab_home, container, false);

        gdv_big_deal = (GridView) rootview.findViewById(R.id.gdv_big_deal);
        gdv_woman_fashion = (GridView) rootview.findViewById(R.id.gdv_woman_fashion);
        gdv_beauty_health = (GridView) rootview.findViewById(R.id.gdv_beauty_health);
        gdv_electronic = (GridView) rootview.findViewById(R.id.gdv_electronic);
        gdv_luggage_bags = (GridView) rootview.findViewById(R.id.gdv_luggage_bags);
        gdv_mobile = (GridView) rootview.findViewById(R.id.gdv_mobile);
        gdv_shoe = (GridView) rootview.findViewById(R.id.gdv_shoe);
        gdv_suggesstion = (GridView) rootview.findViewById(R.id.gdv_suggestion);
        slidingimage = (ImageView) rootview.findViewById(R.id.img_homepage_ads);

        title_big_deals = (RelativeLayout) rootview.findViewById(R.id.title_big_deals);
        title_fashion = (RelativeLayout) rootview.findViewById(R.id.title_fashion);
        title_bag = (RelativeLayout) rootview.findViewById(R.id.title_bag);
        title_shoe = (RelativeLayout) rootview.findViewById(R.id.title_shoe);
        title_beauty = (RelativeLayout) rootview.findViewById(R.id.title_beauty);
        title_phone = (RelativeLayout) rootview.findViewById(R.id.title_phone);
        title_electronic = (RelativeLayout) rootview.findViewById(R.id.title_electronic);
        title_md_suggestion = (RelativeLayout) rootview.findViewById(R.id.title_md_suggestion);

        viewpager = HomeFragment.viewPager;

        TextView big_deal_more = (TextView) rootview.findViewById(R.id.bigdeal_more);
        TextView fashion_more = (TextView) rootview.findViewById(R.id.fashion_more);
        TextView bag_more = (TextView) rootview.findViewById(R.id.bag_more);
        TextView shoe_more = (TextView) rootview.findViewById(R.id.shoe_more);
        TextView beauty_health_more = (TextView) rootview.findViewById(R.id.beauty_health_more);
        TextView phone_more = (TextView) rootview.findViewById(R.id.phone_more);
        TextView electronic_more = (TextView) rootview.findViewById(R.id.electronic_more);
        TextView md_suggestion_more = (TextView) rootview.findViewById(R.id.md_suggestion_more);
        final NetworkUtils utils = new NetworkUtils(getContext());

        swipeContainer = (SwipeRefreshLayout) rootview.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (utils.isConnectingToInternet()) {
                    new DataFetcherTask().execute();
                    condition_refresh = 1;
                } else {
                }

            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        if (utils.isConnectingToInternet()) {
            new DataFetcherTask().execute();
        } else {
        }

        big_deal_more.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                viewpager.setCurrentItem(1);
            }
        });
        fashion_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent clothing = new Intent(getActivity().getBaseContext(), ClothingActivity.class);
                startActivity(clothing);
            }
        });
        bag_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent clothing = new Intent(getActivity().getBaseContext(), BagActivity.class);
                startActivity(clothing);
            }
        });
        shoe_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent clothing = new Intent(getActivity().getBaseContext(), ShoeActivity.class);
                startActivity(clothing);
            }
        });
        beauty_health_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent clothing = new Intent(getActivity().getBaseContext(), CosmeticActivity.class);
                startActivity(clothing);
            }
        });
        phone_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent clothing = new Intent(getActivity().getBaseContext(), PhoneActivity.class);
                startActivity(clothing);
            }
        });
        electronic_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent clothing = new Intent(getActivity().getBaseContext(), ElectronicActivity.class);
                startActivity(clothing);
            }
        });
        md_suggestion_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent clothing = new Intent(getActivity().getBaseContext(), .class);
//                startActivity(clothing);
            }
        });

        return rootview;
    }


    private void AnimateandSlideShow() {
        slidingimage.setImageBitmap(bitmap_hot_item.get(currentimageindex % bitmap_hot_item.size()));
        currentimageindex++;
    }

    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setMessage("loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            serverData = null;

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://www.myanmardealer.com/last_three_json.txt");

            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                serverData = EntityUtils.toString(httpEntity);
                Log.d("response", serverData);

                JSONObject jsonObject = new JSONObject(serverData);
                JSONArray jsonArray_cloth = jsonObject.getJSONArray("FashionClothings");
                JSONArray jsonArray_bag = jsonObject.getJSONArray("FashionBags");
                JSONArray jsonArray_footwear = jsonObject.getJSONArray("FashionFootwears");
                JSONArray jsonArray_beauty = jsonObject.getJSONArray("BeautyEquipments");
                JSONArray jsonArray_phone = jsonObject.getJSONArray("TelephoneAccessories");
                JSONArray jsonArray_electronic = jsonObject.getJSONArray("ElectricalEquipments");
                JSONArray jsonArray_hot_item = jsonObject.getJSONArray("HotItem");
                JSONArray jsonArray_big_deals = jsonObject.getJSONArray("BigDeals");

                arraylist = new ArrayList<HashMap<String, String>>();
                arraylist_bag = new ArrayList<HashMap<String, String>>();
                arraylist_shoe = new ArrayList<HashMap<String, String>>();
                arraylist_beauty = new ArrayList<HashMap<String, String>>();
                arraylist_phone = new ArrayList<HashMap<String, String>>();
                arraylist_electronic = new ArrayList<HashMap<String, String>>();
                arraylist_big_deals = new ArrayList<HashMap<String, String>>();

                for (int i = 0; i < jsonArray_big_deals.length(); i++) {
                    HashMap<String, String> map_big_deals = new HashMap<String, String>();
                    JSONObject jsonObject_big_deals = jsonArray_big_deals.getJSONObject(i);

                    map_big_deals.put("clothingName", jsonObject_big_deals.getString("title"));
                    map_big_deals.put("clothingPrice", jsonObject_big_deals.getString("price"));
                    map_big_deals.put("clothingQuantity", "5");
                    map_big_deals.put("clothingStore_name", jsonObject_big_deals.getString("store_name"));
                    map_big_deals.put("size", jsonObject_big_deals.getString("size"));
                    map_big_deals.put("color", jsonObject_big_deals.getString("color"));
                    map_big_deals.put("description", jsonObject_big_deals.getString("description"));
                    map_big_deals.put("made_by_country", jsonObject_big_deals.getString("made_by_country"));
                    map_big_deals.put("brand", jsonObject_big_deals.getString("brand"));
                    map_big_deals.put("avatar1_edit", jsonObject_big_deals.getString("avatar1_edit"));
                    map_big_deals.put("avatar2_edit", jsonObject_big_deals.getString("avatar2_edit"));
                    map_big_deals.put("avatar3_edit", jsonObject_big_deals.getString("avatar3_edit"));
                    map_big_deals.put("avatar4_edit", jsonObject_big_deals.getString("avatar4_edit"));
                    map_big_deals.put("user_id", jsonObject_big_deals.getString("user_id"));
                    map_big_deals.put("id", jsonObject_big_deals.getString("id"));
                    map_big_deals.put("seller_name", jsonObject_big_deals.getString("seller_name"));

                    arraylist_big_deals.add(map_big_deals);
                }

                for (int i = 0; i < jsonArray_cloth.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    JSONObject jsonObjectClothing = jsonArray_cloth.getJSONObject(i);

                    map.put("clothingName", jsonObjectClothing.getString("title"));
                    map.put("clothingPrice", jsonObjectClothing.getString("price"));
                    map.put("clothingQuantity", "5");
                    map.put("clothingStore_name", jsonObjectClothing.getString("store_name"));
                    map.put("size", jsonObjectClothing.getString("size"));
                    map.put("color", jsonObjectClothing.getString("color"));
                    map.put("description", jsonObjectClothing.getString("description"));
                    map.put("made_by_country", jsonObjectClothing.getString("made_by_country"));
                    map.put("made_with", jsonObjectClothing.getString("made_with"));
                    map.put("gender", jsonObjectClothing.getString("gender"));
                    map.put("brand", jsonObjectClothing.getString("brand"));
                    map.put("avatar1_edit", jsonObjectClothing.getString("avatar1_edit"));
                    map.put("avatar2_edit", jsonObjectClothing.getString("avatar2_edit"));
                    map.put("avatar3_edit", jsonObjectClothing.getString("avatar3_edit"));
                    map.put("avatar4_edit", jsonObjectClothing.getString("avatar4_edit"));
                    map.put("user_id", jsonObjectClothing.getString("user_id"));
                    map.put("id", jsonObjectClothing.getString("id"));
                    map.put("seller_name", jsonObjectClothing.getString("seller_name"));

                    arraylist.add(map);

                }
                for (int i = 0; i < jsonArray_bag.length(); i++) {
                    HashMap<String, String> map_bag = new HashMap<String, String>();
                    JSONObject jsonObject_bag = jsonArray_bag.getJSONObject(i);

                    map_bag.put("clothingName", jsonObject_bag.getString("title"));
                    map_bag.put("clothingPrice", jsonObject_bag.getString("price"));
                    map_bag.put("clothingQuantity", "5");
                    map_bag.put("clothingStore_name", jsonObject_bag.getString("store_name"));
                    map_bag.put("size", jsonObject_bag.getString("size"));
                    map_bag.put("color", jsonObject_bag.getString("color"));
                    map_bag.put("description", jsonObject_bag.getString("description"));
                    map_bag.put("made_by_country", jsonObject_bag.getString("made_by_country"));
                    map_bag.put("brand", jsonObject_bag.getString("brand"));
                    map_bag.put("avatar1_edit", jsonObject_bag.getString("avatar1_edit"));
                    map_bag.put("avatar2_edit", jsonObject_bag.getString("avatar2_edit"));
                    map_bag.put("avatar3_edit", jsonObject_bag.getString("avatar3_edit"));
                    map_bag.put("avatar4_edit", jsonObject_bag.getString("avatar4_edit"));
                    map_bag.put("user_id", jsonObject_bag.getString("user_id"));
                    map_bag.put("id", jsonObject_bag.getString("id"));
                    map_bag.put("seller_name", jsonObject_bag.getString("seller_name"));

                    arraylist_bag.add(map_bag);

                }
                for (int i = 0; i < jsonArray_footwear.length(); i++) {
                    HashMap<String, String> map_footwear = new HashMap<String, String>();
                    JSONObject jsonObject_footwear = jsonArray_footwear.getJSONObject(i);

                    map_footwear.put("clothingName", jsonObject_footwear.getString("title"));
                    map_footwear.put("clothingPrice", jsonObject_footwear.getString("price"));
                    map_footwear.put("clothingQuantity", "5");
                    map_footwear.put("clothingStore_name", jsonObject_footwear.getString("store_name"));
                    map_footwear.put("size", jsonObject_footwear.getString("size"));
                    map_footwear.put("color", jsonObject_footwear.getString("color"));
                    map_footwear.put("description", jsonObject_footwear.getString("description"));
                    map_footwear.put("made_by_country", jsonObject_footwear.getString("made_by_country"));
                    map_footwear.put("brand", jsonObject_footwear.getString("brand"));
                    map_footwear.put("avatar1_edit", jsonObject_footwear.getString("avatar1_edit"));
                    map_footwear.put("avatar2_edit", jsonObject_footwear.getString("avatar2_edit"));
                    map_footwear.put("avatar3_edit", jsonObject_footwear.getString("avatar3_edit"));
                    map_footwear.put("avatar4_edit", jsonObject_footwear.getString("avatar4_edit"));
                    map_footwear.put("user_id", jsonObject_footwear.getString("user_id"));
                    map_footwear.put("id", jsonObject_footwear.getString("id"));
                    map_footwear.put("seller_name", jsonObject_footwear.getString("seller_name"));

                    arraylist_shoe.add(map_footwear);

                }
                for (int i = 0; i < jsonArray_beauty.length(); i++) {
                    HashMap<String, String> map_beauty = new HashMap<String, String>();
                    JSONObject jsonObject_beauty = jsonArray_cloth.getJSONObject(i);

                    map_beauty.put("clothingName", jsonObject_beauty.getString("title"));
                    map_beauty.put("clothingPrice", jsonObject_beauty.getString("price"));
                    map_beauty.put("clothingQuantity", "5");
                    map_beauty.put("clothingStore_name", jsonObject_beauty.getString("store_name"));
                    map_beauty.put("size", jsonObject_beauty.getString("size"));
                    map_beauty.put("color", jsonObject_beauty.getString("color"));
                    map_beauty.put("description", jsonObject_beauty.getString("description"));
                    map_beauty.put("made_by_country", jsonObject_beauty.getString("made_by_country"));
                    map_beauty.put("brand", jsonObject_beauty.getString("brand"));
                    map_beauty.put("avatar1_edit", jsonObject_beauty.getString("avatar1_edit"));
                    map_beauty.put("avatar2_edit", jsonObject_beauty.getString("avatar2_edit"));
                    map_beauty.put("avatar3_edit", jsonObject_beauty.getString("avatar3_edit"));
                    map_beauty.put("avatar4_edit", jsonObject_beauty.getString("avatar4_edit"));
                    map_beauty.put("user_id", jsonObject_beauty.getString("user_id"));
                    map_beauty.put("id", jsonObject_beauty.getString("id"));
                    map_beauty.put("seller_name", jsonObject_beauty.getString("seller_name"));

                    arraylist_beauty.add(map_beauty);

                }
                for (int i = 0; i < jsonArray_phone.length(); i++) {
                    HashMap<String, String> map_phone = new HashMap<String, String>();
                    JSONObject jsonObject_phone = jsonArray_cloth.getJSONObject(i);

                    map_phone.put("clothingName", jsonObject_phone.getString("title"));
                    map_phone.put("clothingPrice", jsonObject_phone.getString("price"));
                    map_phone.put("clothingQuantity", "5");
                    map_phone.put("clothingStore_name", jsonObject_phone.getString("store_name"));
                    map_phone.put("size", jsonObject_phone.getString("size"));
                    map_phone.put("color", jsonObject_phone.getString("color"));
                    map_phone.put("description", jsonObject_phone.getString("description"));
                    map_phone.put("made_by_country", jsonObject_phone.getString("made_by_country"));
                    map_phone.put("brand", jsonObject_phone.getString("brand"));
                    map_phone.put("avatar1_edit", jsonObject_phone.getString("avatar1_edit"));
                    map_phone.put("avatar2_edit", jsonObject_phone.getString("avatar2_edit"));
                    map_phone.put("avatar3_edit", jsonObject_phone.getString("avatar3_edit"));
                    map_phone.put("avatar4_edit", jsonObject_phone.getString("avatar4_edit"));
                    map_phone.put("user_id", jsonObject_phone.getString("user_id"));
                    map_phone.put("id", jsonObject_phone.getString("id"));
                    map_phone.put("seller_name", jsonObject_phone.getString("seller_name"));

                    arraylist_phone.add(map_phone);

                }

                for (int i = 0; i < jsonArray_electronic.length(); i++) {
                    HashMap<String, String> map_electronic = new HashMap<String, String>();
                    JSONObject jsonObject_electronic = jsonArray_cloth.getJSONObject(i);

                    map_electronic.put("clothingName", jsonObject_electronic.getString("title"));
                    map_electronic.put("clothingPrice", jsonObject_electronic.getString("price"));
                    map_electronic.put("clothingQuantity", "5");
                    map_electronic.put("clothingStore_name", jsonObject_electronic.getString("store_name"));
                    map_electronic.put("size", jsonObject_electronic.getString("size"));
                    map_electronic.put("color", jsonObject_electronic.getString("color"));
                    map_electronic.put("description", jsonObject_electronic.getString("description"));
                    map_electronic.put("made_by_country", jsonObject_electronic.getString("made_by_country"));
                    map_electronic.put("brand", jsonObject_electronic.getString("brand"));
                    map_electronic.put("avatar1_edit", jsonObject_electronic.getString("avatar1_edit"));
                    map_electronic.put("avatar2_edit", jsonObject_electronic.getString("avatar2_edit"));
                    map_electronic.put("avatar3_edit", jsonObject_electronic.getString("avatar3_edit"));
                    map_electronic.put("avatar4_edit", jsonObject_electronic.getString("avatar4_edit"));
                    map_electronic.put("user_id", jsonObject_electronic.getString("user_id"));
                    map_electronic.put("id", jsonObject_electronic.getString("id"));
                    map_electronic.put("seller_name", jsonObject_electronic.getString("seller_name"));

                    arraylist_electronic.add(map_electronic);

                }

//                for (int i = 0; i < jsonArray_hot_item.length(); i++) {
//                    JSONObject jsonObject_hot_item = jsonArray_hot_item.getJSONObject(i);
//                    image_string = jsonObject_hot_item.getString("image_data").toString();http://www.myanmardealer.com/order_view_by_seller.txt?user_id=
//
//                    if (image_string.length() > 22) {
//                        String base64_string = image_string.substring(22, image_string.length());
//                        String base64_string_substring = base64_string.substring(1, base64_string.length()-1);
//                        byte[] decodedString = Base64.decode(base64_string_substring, Base64.DEFAULT);
//                        bitmap_hot_item.add(i, BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length));
//                    }
//                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mProgressDialog.dismiss();

            if (condition_refresh == 1) {
                swipeContainer.setRefreshing(false);
                condition_refresh = 0;
            }

            if (arraylist_big_deals != null) {
                if(arraylist_big_deals.size() !=0) {
                    title_big_deals.setVisibility(View.VISIBLE);
                    bigdeal_adapter = new BigDeal_GridViewAdapter(getContext(), arraylist_big_deals, false);
                    gdv_big_deal.setAdapter(bigdeal_adapter);
                }
            }
            if (arraylist != null) {
                if(arraylist.size() !=0) {
                    title_fashion.setVisibility(View.VISIBLE);
                    adapter = new BigDeal_GridViewAdapter(getContext(), arraylist, false);
                    gdv_woman_fashion.setAdapter(adapter);
                }
            }
            if (arraylist_bag != null) {
                if(arraylist_bag.size() !=0) {
                    title_bag.setVisibility(View.VISIBLE);
                    adapter_bag = new BigDeal_GridViewAdapter(getContext(), arraylist_bag, false);
                    gdv_luggage_bags.setAdapter(adapter_bag);
                }
            }
            if (arraylist_shoe != null) {
                if(arraylist_shoe.size() !=0) {
                    title_shoe.setVisibility(View.VISIBLE);
                    adapter_shoe = new BigDeal_GridViewAdapter(getContext(), arraylist_shoe, false);
                    gdv_shoe.setAdapter(adapter_shoe);
                }
            }
            if (arraylist_beauty != null) {
                if(arraylist_beauty.size() !=0) {
                    title_beauty.setVisibility(View.VISIBLE);
                    adapter_beauty = new BigDeal_GridViewAdapter(getContext(), arraylist_beauty, false);
                    gdv_beauty_health.setAdapter(adapter_beauty);
                }
            }
            if (arraylist_phone != null) {
                if(arraylist_phone.size() ==0) {
                    title_phone.setVisibility(View.VISIBLE);
                    adapter_phone = new BigDeal_GridViewAdapter(getContext(), arraylist_phone, false);
                    gdv_mobile.setAdapter(adapter_phone);
                }
            }
            if (arraylist_electronic != null) {
                if(arraylist_electronic.size() ==0) {
                    title_electronic.setVisibility(View.VISIBLE);
                    adapter_electronic = new BigDeal_GridViewAdapter(getContext(), arraylist_electronic, false);
                    gdv_electronic.setAdapter(adapter_electronic);
                }
            }

            final android.os.Handler mHandler = new android.os.Handler();

            // Create runnable for posting
            final Runnable mUpdateResults = new Runnable() {
                public void run() {
                    if(image_string != null) {
                        if (image_string.length() > 22) {
                            AnimateandSlideShow();
                        }
                    }
                }
            };

            int delay = 1000; // delay for 1 sec.
            int period = 15000; // repeat every 4 sec.
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {

                public void run() {
                    mHandler.post(mUpdateResults);
                }

            }, delay, period);

            gdv_big_deal.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(getContext(), ProductDetailActivity.class);
                    i.putExtra("Position", position);
                    i.putExtra("come_from", "tab_home_big_deal");
                    i.putExtra("category", "big_deal");
                    startActivity(i);
                }
            });
            gdv_woman_fashion.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(getContext(), ProductDetailActivity.class);
                    i.putExtra("Position", position);
                    i.putExtra("come_from", "tab_home_fashion");
                    i.putExtra("category", "clothing");
                    startActivity(i);
                }
            });

            gdv_beauty_health.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(getContext(), ProductDetailActivity.class);
                    i.putExtra("Position", position);
                    i.putExtra("come_from", "tab_home_beauty");
                    i.putExtra("category", "cosmetic");
                    startActivity(i);
                }
            });
            gdv_electronic.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(getContext(), ProductDetailActivity.class);
                    i.putExtra("Position", position);
                    i.putExtra("come_from", "tab_home_electronic");
                    i.putExtra("category", "electronic");
                    startActivity(i);
                }
            });
            gdv_luggage_bags.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(getContext(), ProductDetailActivity.class);
                    i.putExtra("Position", position);
                    i.putExtra("come_from", "tab_home_bag");
                    i.putExtra("category", "bag");
                    startActivity(i);
                }
            });
            gdv_mobile.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(getContext(), ProductDetailActivity.class);
                    i.putExtra("Position", position);
                    i.putExtra("come_from", "tab_home_mobile");
                    i.putExtra("category", "phone");
                    startActivity(i);
                }
            });
            gdv_shoe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(getContext(), ProductDetailActivity.class);
                    i.putExtra("Position", position);
                    i.putExtra("come_from", "tab_home_shoe");
                    i.putExtra("category", "shoe");
                    startActivity(i);
                }
            });
            gdv_suggesstion.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(getContext(), ProductDetailActivity.class);
                    i.putExtra("Position", position);
                    i.putExtra("come_from", "tab_home_suggesstion");
                    i.putExtra("category", "suggesstion");
                    startActivity(i);
                }
            });
        }
    }

}
