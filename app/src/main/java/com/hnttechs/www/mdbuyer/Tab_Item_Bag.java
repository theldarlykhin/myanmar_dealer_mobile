package com.hnttechs.www.mdbuyer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 7/25/16.
 */
public class Tab_Item_Bag extends Fragment {

    ArrayList<HashMap<String, String>> arraylist;
    int position;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootview = inflater.inflate(R.layout.tab_item_bag, container, false);

        TextView txt_color = (TextView)rootview.findViewById(R.id.txt_color);
        TextView txt_size = (TextView)rootview.findViewById(R.id.txt_size);
        TextView txt_made_by_country = (TextView)rootview.findViewById(R.id.txt_made_by_country);
        TextView txt_made_with = (TextView)rootview.findViewById(R.id.txt_made_with);
        TextView txt_bag_type = (TextView)rootview.findViewById(R.id.txt_bag_type);
        TextView txt_brand = (TextView)rootview.findViewById(R.id.txt_brand);



        arraylist = ProductDetailActivity.serverdata_Arraylist;
        position = ProductDetailActivity.position;
        txt_color.setText(arraylist.get(position).get("color"));
        txt_size.setText(arraylist.get(position).get("size"));
        txt_made_by_country.setText(arraylist.get(position).get("made_by_country"));
        txt_made_with.setText(arraylist.get(position).get("made_with"));
        txt_bag_type.setText(arraylist.get(position).get("brand"));
        txt_brand.setText(arraylist.get(position).get("brand"));


        return rootview;
    }
}
