package com.hnttechs.www.mdbuyer;

import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 7/25/16.
 */
public class Tab_Item_Description extends Fragment {

    int position;
    static String serverData;
    static String description_text;
    static String description_image1;
    static String description_image2;
    static String description_image3;
    static String description_image4;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootview = inflater.inflate(R.layout.tab_item_description, container, false);

        TextView txt_desc = (TextView) rootview.findViewById(R.id.lbl_description);
        ImageView img_desc1 = (ImageView) rootview.findViewById(R.id.img_desc1);
        ImageView img_desc2 = (ImageView) rootview.findViewById(R.id.img_desc2);
        ImageView img_desc3 = (ImageView) rootview.findViewById(R.id.img_desc3);
        ImageView img_desc4 = (ImageView) rootview.findViewById(R.id.img_desc4);

        new DataFetcherTask().execute();
        txt_desc.setText(description_text);

        if (description_image1 != null && !description_image1.isEmpty() && !description_image1.equals("null")) {
            String base64_string1 = description_image1.substring(22, description_image1.length());
            byte[] decodedString1 = Base64.decode(base64_string1, Base64.DEFAULT);
            img_desc1.setImageBitmap(BitmapFactory.decodeByteArray(decodedString1, 0, decodedString1.length));
        }

        if (description_image2 != null && !description_image2.isEmpty() && !description_image2.equals("null")) {
            String base64_string2 = description_image2.substring(22, description_image2.length());
            byte[] decodedString2 = Base64.decode(base64_string2, Base64.DEFAULT);
            img_desc2.setImageBitmap(BitmapFactory.decodeByteArray(decodedString2, 0, decodedString2.length));


        }
        if (description_image3 != null && !description_image3.isEmpty() && !description_image3.equals("null")) {
            String base64_string3 = description_image3.substring(22, description_image3.length());
            byte[] decodedString3 = Base64.decode(base64_string3, Base64.DEFAULT);
            img_desc3.setImageBitmap(BitmapFactory.decodeByteArray(decodedString3, 0, decodedString3.length));


        }
        if (description_image4 != null && !description_image4.isEmpty() && !description_image4.equals("null")) {
            String base64_string4 = description_image4.substring(22, description_image4.length());
            byte[] decodedString4 = Base64.decode(base64_string4, Base64.DEFAULT);
            img_desc4.setImageBitmap(BitmapFactory.decodeByteArray(decodedString4, 0, decodedString4.length));
        }

        return rootview;
    }


    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            serverData = null;

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://www.myanmardealer.com/store_desc.txt");
            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                serverData = EntityUtils.toString(httpEntity);
                Log.d("response", serverData);

                JSONObject jsonObject = new JSONObject(serverData);
                JSONArray jsonArray = jsonObject.getJSONArray("Rating");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObjectrating = jsonArray.getJSONObject(i);

                    if (ProductDetailActivity.resultp.get("clothingStore_name").equals(jsonObjectrating.getString("id"))) {
                        description_text = jsonObjectrating.getString("description");
                        description_image1 = jsonObjectrating.getString("description_image1");
                        description_image2 = jsonObjectrating.getString("description_image2");
                        description_image3 = jsonObjectrating.getString("description_image3");
                        description_image4 = jsonObjectrating.getString("description_image4");
                    }
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }


}
