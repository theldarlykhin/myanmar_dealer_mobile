package com.hnttechs.www.mdbuyer;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dell on 7/25/16.
 */
public class Tab_Item_Rating extends Fragment {

    Rating_ListAdapter item_spec_listAdapter;
    static String serverData;
    ArrayList<HashMap<String, String>> arraylist;
    static ListView lv_rating;
    static TextView txt_comment;

    ProgressDialog dialog;
    static SharedPreferences mPreferences;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootview = inflater.inflate(R.layout.tab_item_rating, container, false);

        lv_rating = (ListView) rootview.findViewById(R.id.lv_rating);
        txt_comment = (TextView)rootview.findViewById(R.id.txt_comment);
        Button btn_comment = (Button)rootview.findViewById(R.id.btn_comment);

        mPreferences = getActivity().getSharedPreferences("CurrentUser", Context.MODE_PRIVATE);

        new DataFetcherTask().execute();

        btn_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendPostRequest(mPreferences.getString("UserId","0"),txt_comment.getText().toString(),ProductDetailActivity.product_id);
//                dialog = ProgressDialog.show(getActivity().getBaseContext(), "aok", "Sending...", true);
//                dialog.show();
            }
        });
        return rootview;
    }


    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            serverData = null;

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://www.myanmardealer.com/get_rating.txt");
            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                serverData = EntityUtils.toString(httpEntity);
                Log.d("response", serverData);

                JSONObject jsonObject = new JSONObject(serverData);
                JSONArray jsonArray = jsonObject.getJSONArray("Rating");
                arraylist = new ArrayList<HashMap<String, String>>();

                for (int i = 0; i < jsonArray.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    JSONObject jsonObjectrating = jsonArray.getJSONObject(i);

                    if (ProductDetailActivity.product_id.equals(jsonObjectrating.getString("product_id"))) {
                        map.put("user_id", jsonObjectrating.getString("user_id"));
                        map.put("message", jsonObjectrating.getString("message"));
                        map.put("product_id", jsonObjectrating.getString("product_id"));
                    }
                    arraylist.add(map);
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            item_spec_listAdapter = new Rating_ListAdapter(getContext(), arraylist);
            lv_rating.setAdapter(item_spec_listAdapter);
        }
    }



    private void sendPostRequest(String UserId, String Message, String ProductId) {

        class SendPostReqAsyncTask extends AsyncTask<Object, Void, String> {

            @Override
            protected String doInBackground(Object... params) {

                String paramUserId = (String) params[0];
                String paramMessage = (String) params[1];
                String paramProductId = (String) params[2];

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost("http://www.myanmardealer.com/to_send_rating");

                BasicNameValuePair userIdBasicNameValuePair = new BasicNameValuePair("user_id", paramUserId);
                BasicNameValuePair messageBasicNameValuePAir = new BasicNameValuePair("message", paramMessage.toString());
                BasicNameValuePair productIdBasicNameValuePAir = new BasicNameValuePair("product_id", paramProductId.toString());

                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(userIdBasicNameValuePair);
                nameValuePairList.add(messageBasicNameValuePAir);
                nameValuePairList.add(productIdBasicNameValuePAir);

                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);
                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        InputStream inputStream = httpResponse.getEntity().getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;

                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
                        return stringBuilder.toString();
                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
//                dialog.dismiss();
                txt_comment.setText("");
                new DataFetcherTask().execute();
            }
        }
        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(UserId, Message, ProductId);
    }



}

