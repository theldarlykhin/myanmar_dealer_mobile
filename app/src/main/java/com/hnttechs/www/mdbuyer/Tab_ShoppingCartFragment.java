package com.hnttechs.www.mdbuyer;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dell on 4/9/16.
 */
public class Tab_ShoppingCartFragment extends Fragment {

    ArrayList<HashMap<String, String>> listData;
    private ShoppingCart_GridViewAdapter mProductAdapter;
    static TextView total_price;
    static CheckBox chk_select_all;
    static int count=0;
    static List<String> product_id_array;
    static List<String> qty_array;
    static List<String> product_name_array;
    static HashMap<String, String> clothing;
    ViewPager viewpager;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.tab_shoppingcart, container, false);



        viewpager = HomeFragment.viewPager;

        listData = ShoppingCartHelper.getCartList();
        count = ShoppingCartHelper.getitemcount();

//        for(int i=0; i<listData.size(); i++) {
//            listData.get(i).selected = false;
//        }

        product_id_array= new ArrayList<String>();
        qty_array= new ArrayList<String>();
        product_name_array= new ArrayList<String>();

        final ListView lv_cartbyshopName = (ListView) rootview.findViewById(R.id.lvcart_by_shopname);

        Button btn_order = (Button)rootview.findViewById(R.id.btn_order);
        chk_select_all = (CheckBox)rootview.findViewById(R.id.chk_select_all);

        total_price = (TextView)rootview.findViewById(R.id.txt_totalprice);


        mProductAdapter = new ShoppingCart_GridViewAdapter(getActivity().getBaseContext(),listData, true);
        lv_cartbyshopName.setAdapter(mProductAdapter);

        lv_cartbyshopName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
//                Intent productDetailsIntent = new Intent(getBaseContext(), ProductDetailActivity.class);
//                productDetailsIntent.putExtra(ShoppingCartHelper.PRODUCT_INDEX, position);
//                startActivity(productDetailsIntent);
            }
        });

        for (int j = 0; j<count;j++){
                product_id_array.add(j, ShoppingCartHelper.getProductId(listData.get(j)));
                qty_array.add(j, "" + ShoppingCartHelper.getProductQuantity(listData.get(j)) + "");
//                product_name_array.add(j, listData.get(j).get("title"));

        }


        btn_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getBaseContext(), Activity_Customer_Info.class);
                i.putStringArrayListExtra("product_id", (ArrayList<String>)product_id_array);
                i.putStringArrayListExtra("qty", (ArrayList<String>)qty_array);
//                i.putStringArrayListExtra("product_name", (ArrayList<String>)product_name_array);
                startActivity(i);

                viewpager.setCurrentItem(1);
            }
        });

        chk_select_all.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ShoppingCart_GridViewAdapter.chk_order.setChecked(true);
            }
        });


        return  rootview;
    }


}
