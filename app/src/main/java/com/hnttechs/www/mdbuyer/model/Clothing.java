package com.hnttechs.www.mdbuyer.model;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

/**
 * Created by dell on 4/8/16.
 */
public class Clothing {

    private int id;
    private String title;
    private int price;
    private int quantity;
    private String size;
    private String made_by_country;
    private String made_with;
    private String description;
    private String gender;
    private String brand;
    private int seller_id;
    private String color;
    private String store_name;
    private String avater1;
    private String avater2;
    private String avater3;
    private String avater4;
    private String avater5;
    public boolean selected;

    public Clothing() {
    }


    public Clothing(String title, String description,int price) {
        this.title = title;
        this.description = description;
        this.price = price;
    }


    public Clothing(String title, int price, int quantity,String size, String made_by_country,
                    String made_with, String description, String gender, String brand, int seller_id,
                    String color, String store_name, String avater1, String avater2, String avater3,
                    String avater4, String avater5){
        this.title = title;
        this.price =price;
        this.quantity=quantity;
        this.size=size;
        this.made_by_country=made_by_country;
        this.made_with=made_with;
        this.description=description;
        this.gender=gender;
        this.brand=brand;
        this.seller_id=seller_id;
        this.color=color;
        this.store_name=store_name;
        this.avater1=avater1;
        this.avater2=avater2;
        this.avater3=avater3;
        this.avater4=avater4;
        this.avater5=avater5;
    }


    public Clothing(int id, String title, int price, int quantity,String size, String made_by_country,
                    String made_with, String description, String gender, String brand, int seller_id,
                    String color, String store_name, String avater1, String avater2, String avater3,
                    String avater4, String avater5){
        this.id = id;
        this.title = title;
        this.price =price;
        this.quantity=quantity;
        this.size=size;
        this.made_by_country=made_by_country;
        this.made_with=made_with;
        this.description=description;
        this.gender=gender;
        this.brand=brand;
        this.seller_id=seller_id;
        this.color=color;
        this.store_name=store_name;
        this.avater1=avater1;
        this.avater2=avater2;
        this.avater3=avater3;
        this.avater4=avater4;
        this.avater5=avater5;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getMade_by_country() {
        return made_by_country;
    }

    public void setMade_by_country(String made_by_country) {
        this.made_by_country = made_by_country;
    }


    public String getMade_with() {
        return made_with;
    }

    public void setMade_with(String made_with) {
        this.made_with = made_with;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getSeller_id() {
        return seller_id;
    }

    public void setSeller_id(int seller_id) {
        this.seller_id = seller_id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }


    public String getAvater1() {
        return avater1;
    }

    public void setAvater1(String avater1) {
        this.avater1 = avater1;
    }


    public String getAvater2() {
        return avater2;
    }

    public void setAvater2(String avater2) {
        this.avater2 = avater2;
    }


    public String getAvater3() {
        return avater3;
    }

    public void setAvater3(String avater3) {
        this.avater3 = avater3;
    }

    public String getAvater4() {
        return avater4;
    }

    public void setAvater4(String avater4) {
        this.avater4 = avater4;
    }

    public String getAvater5() {
        return avater5;
    }

    public void setAvater5(String avater5) {
        this.avater5 = avater5;
    }

}
