package com.hnttechs.www.mdbuyer;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 5/4/16.
 */
public class order_list_ListAdapter extends BaseAdapter {


    // Declare Variables
    Context c;
    ArrayList<HashMap<String, String>> serverdata_arraylist;

    public order_list_ListAdapter(Context context, ArrayList<HashMap<String, String>>  arraylist) {
        c = context;
        serverdata_arraylist= arraylist;
    }

    @Override
    public int getCount() {
        return serverdata_arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {


        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.order_list_listitem, parent, false);

            TextView customer_name = (TextView)convertView.findViewById(R.id.customer_name);
            TextView product_name = (TextView)convertView.findViewById(R.id.product_name);
            TextView quantity = (TextView)convertView.findViewById(R.id.quantity);

            customer_name.setText(serverdata_arraylist.get(position).get("customer_name"));
            product_name.setText(serverdata_arraylist.get(position).get("product_name"));
            quantity.setText(serverdata_arraylist.get(position).get("quantity"));

        }

        return convertView;
    }
}
