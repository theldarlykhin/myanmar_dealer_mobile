package com.hnttechs.www.mdbuyer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 5/4/16.
 */
public class seller_order_list_ListAdapter extends BaseAdapter {


    // Declare Variables
    Context c;
    ArrayList<HashMap<String, String>> serverdata_arraylist;

    public seller_order_list_ListAdapter(Context context, ArrayList<HashMap<String, String>> arraylist) {
        c = context;
        serverdata_arraylist= arraylist;
    }

    @Override
    public int getCount() {
        return serverdata_arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {


        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.order_list_listitem, parent, false);

            TextView order_status = (TextView)convertView.findViewById(R.id.order_status);
            TextView customer_name = (TextView)convertView.findViewById(R.id.customer_name);
            TextView customer_email = (TextView)convertView.findViewById(R.id.customer_email);
            TextView customer_phone = (TextView)convertView.findViewById(R.id.customer_phone);
            TextView customer_address = (TextView)convertView.findViewById(R.id.customer_address);
            TextView customer_city = (TextView)convertView.findViewById(R.id.customer_city);
            TextView customer_township = (TextView)convertView.findViewById(R.id.customer_township);

            order_status.setText(serverdata_arraylist.get(position).get("order_status"));
            customer_name.setText(serverdata_arraylist.get(position).get("customer_name"));
            customer_email.setText(serverdata_arraylist.get(position).get("customer_email"));
            customer_phone.setText(serverdata_arraylist.get(position).get("customer_phone"));
            customer_address.setText(serverdata_arraylist.get(position).get("customer_city"));
            customer_city.setText(serverdata_arraylist.get(position).get("customer_township"));
            customer_township.setText(serverdata_arraylist.get(position).get("customer_address"));
        }

        return convertView;
    }
}
